package Backend.Sockets;

import Backend.DataFrame1;
import Backend.Sockets.Client;
import Backend.Threads.CalcSeverThread;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
    ArrayList<Socket> arraySocket = null;
    int port=6670;
    String host = "localhost";
    public void startServer() {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(6666);
        } catch (IOException e) {
            System.out.println("Could not listen on port: 6666");
            System.exit(-1);
        }
        Socket clientSocket = null;
        try {
            clientSocket = serverSocket.accept();
        } catch (IOException e) {
            System.out.println("Accept failed: 6666");
            System.exit(-1);
        }
        try {
            ObjectInputStream ois = new ObjectInputStream(clientSocket.getInputStream());
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(clientSocket.getOutputStream());
            Client c = (Client)ois.readObject();
            ExecutorService executorService = Executors.newFixedThreadPool(1);
            DataFrame1 dftoReturn = new DataFrame1(c.d.getColnames(), c.d.getColnamesTypes());
            for (DataFrame1 df : c.d.getMap().values()){
                df.setGroupByColnames(c.d.getGroupByColnames());
                df.setOp(c.getOp());
                Runnable runnable = new CalcSeverThread(df, dftoReturn );
                executorService.execute(runnable);
            }
            executorService.shutdown();
            while (!executorService.isTerminated()){
            }
            objectOutputStream.writeObject(dftoReturn);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void connectTo(){
        Socket s;
        for(int i=0; i<8;i++){
            try{
                s = new Socket(host, port+i);
                arraySocket.add(s);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}