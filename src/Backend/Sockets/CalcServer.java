package Backend.Sockets;

import Backend.DataFrame1;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class CalcServer {
    private int port;
    public CalcServer(int port){
        this.port=port;
    }

    public void startServer(){
        ServerSocket serverSocket = null;
        try{
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Socket clientSocket = null;
        try{
            clientSocket = serverSocket.accept();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try{
            ObjectInputStream ois = new ObjectInputStream(clientSocket.getInputStream());
            ObjectOutputStream ous = new ObjectOutputStream(clientSocket.getOutputStream());
            DataFrame1 df = (DataFrame1)ois.readObject();
            ous.writeObject(df.operations(df.getOp()));
            clientSocket.close();
            startServer();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
