package Backend.Interfaces;

import Backend.DataFrame1;
import Backend.Interfaces.Applyable;

public interface GroupBy{
	DataFrame1 max();
	DataFrame1 min();
	DataFrame1 mean();
	DataFrame1 std();
	DataFrame1 sum();
	DataFrame1 var();
	DataFrame1 apply(Applyable a);
}
