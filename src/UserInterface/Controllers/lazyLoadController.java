package UserInterface.Controllers;

import Backend.DataFrame1;
import Backend.ValueClasses.IntegerValue;
import Backend.ValueClasses.Value;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import java.util.ArrayList;

public class lazyLoadController {
    private DataFrame1 df;
    private HBox hbox;
    private ArrayList<ObservableList<Value>> itemList = new ArrayList<ObservableList<Value>>();
    private ArrayList<ObservableList<Value>> bigData = new ArrayList<ObservableList<Value>>();
    int start = 0;
    int step = 6;
    @FXML
    private AnchorPane anchorpane;
    public lazyLoadController(DataFrame1 df){
        this.df = df;
    }
    public void bindscroll(){
        ScrollBar scrollbar = new ScrollBar();
        scrollbar.setOrientation(Orientation.VERTICAL);
        ListView listView;
        scrollbar.setMax(1);
        for(Node n : hbox.getChildren()){
            listView = (ListView) n;
            for (Node node : listView.lookupAll(".scroll-bar")) {
                if (node instanceof ScrollBar) {
                    ScrollBar bar = (ScrollBar) node;
                    if (bar.getOrientation().equals(Orientation.VERTICAL)) {
                        bar.valueProperty().bindBidirectional(scrollbar.valueProperty());

                    }
                }
            }
        }
        hbox.getChildren().add(scrollbar);

        scrollbar.valueProperty().addListener((observable, oldValue, newValue) -> {
            double position = newValue.doubleValue();
            if (position == scrollbar.getMax()) {
                if (step <= bigData.get(0).size()) {
                    try {
                    for (int i=0; i <itemList.size(); i++){
                        start = step;
                        step += 50;
                    itemList.get(i).addAll(bigData.get(i).subList(start, step));
                    }
                    scrollbar.setValue(scrollbar.getMax()*0.9);}
                    catch (IndexOutOfBoundsException e){
                        for (int i=0; i <itemList.size(); i++){
                            itemList.get(i).addAll(bigData.get(i).subList(start, df.size()));
                        }
                    }
                }
            }
        });

    }
    @FXML
    void initialize(){
        hbox = new HBox();
        ListView <Value>lv ;
        ArrayList<Value> arrayList = new ArrayList<Value>();
        for (int i = 1; i<df.size()+1; i++){
            arrayList.add(new IntegerValue(i));
        }
        bigData.add(FXCollections.observableArrayList(arrayList));
        itemList.add(FXCollections.observableArrayList(bigData.get(0).subList(start,step)));
        lv = new ListView<Value>();
        lv.setPrefWidth(120);
        lv.setItems(itemList.get(0));
        hbox.getChildren().add(lv);


        int i = 1;
        for(String s: df.getKolumny()){
            bigData.add(FXCollections.observableArrayList(df.get(s).getArraylist()));
            itemList.add(FXCollections.observableArrayList(bigData.get(i).subList(start,step)));
            lv = new ListView<Value>();
            lv.setItems(itemList.get(i));
            i++;
//            lv.setCellFactory(Value);
//            lv.setOnMouseClicked(new EventHandler<Event>() {
//
//                @Override
//                public void handle(Event event) {
//System.out.println("dadad");
//
//                }
//
//            });
            hbox.getChildren().add(lv);
        }
        anchorpane.getChildren().add(hbox);
    }
}
