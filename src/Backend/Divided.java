package Backend;

import Backend.Interfaces.Applyable;
import Backend.Interfaces.GroupBy;
import Backend.Interfaces.SortByValue;
import Backend.Threads.OperationThread;
import Backend.ValueClasses.DateTimeValue;
import Backend.ValueClasses.StringValue;
import Backend.ValueClasses.Value;

import java.io.Serializable;
import java.util.*;

import static java.lang.Thread.sleep;

/**
 * @author Sonia Klasa przechowujaca podzielone DataFrame1 wedlug klucza
 *         wewnatrz mapy
 */
public class Divided implements GroupBy, Serializable {

    private Map<List<Value>, DataFrame1> map;
    private String[] groupByColnames;
    private String[] colnames;
	private Class<? extends Value>[] colnamesTypes;
	private DataFrame1 notGrouped;

	/**
	 * Tworzy pusta mape, ktora bedzie przechowywala pogrupowane DataFrame1
	 */
	public Divided(String[] groupByColnames, DataFrame1 d) {
		notGrouped = d;
		map = new LinkedHashMap<List<Value>, DataFrame1>();
		this.groupByColnames = groupByColnames;
		this.colnames = d.getKolumny();
		this.colnamesTypes = d.getTypy();
		for (int i = 0; i < d.size(); i++) {
			List<Value> l = new LinkedList<Value>();
			for (String s : groupByColnames) {
				l.add(d.get(s).getValue(i));
				// System.out.print(i+ " ");
				// System.out.print(l + " ");
			}
				if (map.containsKey(l)) {
					//System.out.println(l);
					//System.out.println("Przed dodaniu:" + l + "   "+ i);
					map.get(l).addrow(d.getRow(i));
				} else {
					map.put(l, d.iloc(i));
					//System.out.println("Po dodaniu:" + l + "   "+ i);
				}
			
		}
		System.out.println("Udalo sie pogrupowac !!!!");
	}
    public static void sendToServer(){

    }
    public Map<List<Value>, DataFrame1> getMap() {
        return map;
    }

    public String[] getGroupByColnames() {
        return groupByColnames;
    }

    public String[] getColnames() {
        return colnames;
    }

    public Class<? extends Value>[] getColnamesTypes() {
        return colnamesTypes;
    }

    /**
	 * Metoda odpowiada za wyswietlanie DataFrame1 z mapy
	 */
	public String toString() {
		String out = "";
		for (List<Value> klucz : map.keySet()) {
			out += klucz.toString() + "\n" + map.get(klucz).toString() + "\n";
		}
		return out;
	}

	/**
	 * Metoda zwraca nowa DataFrame1 stworzana na podstawie pol o najmniejszej
	 * wartosci z kazdej zgrupowanej dataFrame1
	 */
	@Override
	public DataFrame1 min() {
		// i odpowiada za iterowanie po tablicy gdzie zapisywane sa wartosci jednego
		// wiersza
		// df pusty DataFrame1 z nazwami kolumn i ich typami
		DataFrame1 df = new DataFrame1(colnames, colnamesTypes);
		DataFrame1 toSort = null;
		// petla ktora przechodzi po kazdej DataFrame1 w obiekcie klasy Divided
		// zapisanym w mapie
		for (List<Value> key : map.keySet()) {
			// petla ktora przechodzi po kazdej Kolumnie w DataFrame1 a nastepnie sortuje
			// kolumne w kolejnosci rosnacej
			toSort = new DataFrame1(map.get(key));
			for (String keyKolumn : map.get(key).getMap().keySet()) {
				Collections.sort(toSort.get(keyKolumn).getArraylist(), new SortByValue());
			}
			df.addrow(toSort.getRow(0));
		}

		return df;
	}

	public DataFrame1 minThread() {
		DataFrame1 df = new DataFrame1(colnames, colnamesTypes);
		DataFrame1 toSort = null;
		Thread t = null;
		ArrayList<Thread> opList = new ArrayList<>();
		for (List<Value> key : map.keySet()) {
			t =new Thread( new OperationThread(df, new DataFrame1(map.get(key)), key.toString(), Operation.MIN));
			t.start();
			opList.add(t);
		}
		for(Thread tread : opList){
			try {
				tread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return df;
	}

	/**
	 * Analogicznie do min() Metoda zwraca nowa DataFrame1 stworzana na podstawie
	 * pol o najwiekszej wartosci z kazdej zgrupowanej dataFrame1
	 */
	@Override
	public DataFrame1 max() {
		DataFrame1 df = new DataFrame1(colnames, colnamesTypes);
		DataFrame1 toSort = null;
		// petla ktora przechodzi po kazdej DataFrame1 w obiekcie klasy Divided
		// zapisanym w mapie
		for (List<Value> key : map.keySet()) {
			// petla ktora przechodzi po kazdej Kolumnie w DataFrame1 a nastepnie sortuje
			// kolumne w kolejnosci rosnacej
			toSort = new DataFrame1(map.get(key));
			for (String keyKolumn : map.get(key).getMap().keySet()) {
				Collections.sort(toSort.get(keyKolumn).getArraylist(), new SortByValue());
			}
			df.addrow(toSort.getRow(toSort.size() - 1));
		}

		return df;
	}

	public DataFrame1 maxThread() {
		DataFrame1 df = new DataFrame1(colnames, colnamesTypes);
		Thread t = null;
		ArrayList<Thread> opList = new ArrayList<>();
		for (List<Value> key : map.keySet()) {
			t =new Thread( new OperationThread(df, new DataFrame1(map.get(key)), key.toString(), Operation.MAX));
			t.start();
			opList.add(t);
		}
		for(Thread tread : opList){
			try {
				tread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return df;
	}

	/**
	 * Metoda zwraca nowa DataFrame1 stworzana na podstawie sredniej kolumn kazdej
	 * DataFrame w tej klasie
	 */
	@Override
	public DataFrame1 mean() {
		int i = 0;
		// df pusty DataFrame1 z nazwami kolumn i ich typami
		DataFrame1 df = new DataFrame1(colnames, colnamesTypes);
		// row - tablicy gdzie zapisywane sa wartosci jednego wiersza
		Value[] row = new Value[colnames.length];
		// petla ktora przechodzi po kazdej DataFrame1 w obiekcie klasy Divided
		// zapisanym w mapie
		for (List<Value> klucz : map.keySet()) {
			// suma- srednia danej kolumna przechowywana obekcie typu jakiego jest dana
			// kolumna
			Value suma = null;
			// petla ktora przechodzi po kazdej Kolumnie w DataFrame1 a nastepnie dodaje jej
			// najmniejsza wartosc do row
			for (String keykolumn : map.get(klucz).getMap().keySet()) {

				if (Arrays.asList(groupByColnames).contains(keykolumn)) {
					// System.out.println(Arrays.asList(colnames).contains(keykolumn) +" " +
					// colnames[0] + " "+ colnames[1]+ " "+keykolumn);
					row[i] = map.get(klucz).map.get(keykolumn).getArraylist().get(0);
					i++;
					continue;
				}
				// liczy srednia tylko dla kolumn ktore nie sa typu String albo DataTime, w
				// przeciwnym razie przepisujemy tylko ich wartosc
				if (!(Value.getInstance(map.get(klucz).get(keykolumn).getTyp()) instanceof DateTimeValue)
						&& !(Value.getInstance(map.get(klucz).get(keykolumn).getTyp()) instanceof StringValue)) {
					// nadanie zmiennej suma wartosci poczatkowej 0 i odpowiedniego typu na
					// podstawie typu kolumny
					suma = Value.getInstance(map.get(klucz).get(keykolumn).getTyp()).create("0");
					// petla przechodzi przez kazdy element kolumny i dodaje jego wartosc do
					// zmiennej suma
					for (Value elem : map.get(klucz).get(keykolumn).getArraylist()) {
						suma.add(elem);
					}
					// dzielimy suma przez ilosc wierszy w kolumnie i otrzymujemy srednia
					suma.div(Value.getInstance(map.get(klucz).get(keykolumn).getTyp())
							.create(String.valueOf(map.get(klucz).size())));
					// dodaje srednia do tablicy z wierszem
					row[i] = suma;
					i++;
					// Jesli kolumna typu DataTimeFormat to nie wykonujemy dzialan tylko zapisujemy
					// obiekt StringValue z pustym stringiem
				} else if (Value.getInstance(map.get(klucz).get(keykolumn).getTyp()) instanceof DateTimeValue) {
					row[i] = Value.getInstance(StringValue.class).create("		");
					i++;
				} else {
					row[i] = Value.getInstance(StringValue.class).create("		");
					i++;
//					row[i] = map.get(klucz).get(keykolumn).getArraylist().get(0);
//					i++;
				}
			}
			// dodawanie wiersza ze srednimi z danej DataFrame do nowej DataFrame
			df.addrow(row);
			i = 0;
		}
		return df;
	}
    public DataFrame1 meanThread() {
        DataFrame1 df = new DataFrame1(colnames, colnamesTypes);
        Thread t = null;
        ArrayList<Thread> opList = new ArrayList<>();
        for (List<Value> key : map.keySet()) {
            t =new Thread( new OperationThread(df, new DataFrame1(map.get(key)), key.toString(), Operation.MEAN, groupByColnames));
            t.start();
            opList.add(t);
        }
        for(Thread tread : opList){
            try {
                tread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return df;
    }
	/**
	 * metoda liczy odchylenie standardowe dla kazdej zgrupowanej DataFrame i
	 * zapisuje wynik w nowej DF
	 */
	@Override
	public DataFrame1 std() {
		int i = 0;
		DataFrame1 df = new DataFrame1(colnames, colnamesTypes);
		Value[] row = new Value[colnames.length];
		// petla ktora przechodzi po kazdej DataFrame1 w obiekcie klasy Divided
		// zapisanym w mapie
		for (List<Value> klucz : map.keySet()) {
			// suma przechowuje srednia danej kolumny
			Value suma = null;
			// war przechowuje wynik odchylenia
			Value war = null;
			// std odpowiada za pierwiastkowanie wariancji, bedzie mialo wartosc 0.5
			Value std = null;
			// petla ktora przechodzi po kazdej Kolumnie w DataFrame1 a nastepnie oblicza
			// jej mean i na podstawie tego odchylenie
			for (String keykolumn : map.get(klucz).map.keySet()) {
				if (Arrays.asList(groupByColnames).contains(keykolumn)) {
					// System.out.println(Arrays.asList(colnames).contains(keykolumn) +" " +
					// colnames[0] + " "+ colnames[1]+ " "+keykolumn);
					row[i] = map.get(klucz).get(keykolumn).getArraylist().get(0);
					i++;
					continue;
				}
				// liczyby odchylenie tylko dla kolumn ktore nie sa typu String albo DataTime, w
				// przeciwnym razie przepisujemy tylko ich wartosc
				if (!(Value.getInstance(map.get(klucz).get(keykolumn).getTyp()) instanceof DateTimeValue)
						&& !(Value.getInstance(map.get(klucz).get(keykolumn).getTyp()) instanceof StringValue)) {
					// nadawanie wartosci 0 dla suma i war oraz tworzenie ich dla typu jakiego jest
					// kolumna
					suma = Value.getInstance(map.get(klucz).get(keykolumn).getTyp()).create("0");
					war = Value.getInstance(map.get(klucz).get(keykolumn).getTyp()).create("0");
					// std uzywana do podnoszenia wariancji do 0.5 i jest tego typu co kolumna
					std = Value.getInstance(map.get(klucz).get(keykolumn).getTyp()).create("0.5");
					// liczenie sumy kolumny
					for (Value elem : map.get(klucz).get(keykolumn).getArraylist()) {
						suma.add(elem);
					}
					// liczenie sredniej kolumny
					suma.div(Value.getInstance(map.get(klucz).get(keykolumn).getTyp())
							.create(String.valueOf(map.get(klucz).size())));
					// liczenie wariancji kolumny
					for (Value elem : map.get(klucz).get(keykolumn).getArraylist()) {
                        try {
                            war.add((elem.clone().sub(suma)).mul(elem.clone().sub(suma)));
                        } catch (CloneNotSupportedException e) {
                            e.printStackTrace();
                        }
                    }
					war.div(Value.getInstance(map.get(klucz).get(keykolumn).getTyp())
							.create(String.valueOf(map.get(klucz).size())));
					war.pow(std);
					// dodawania obliczonej wartosci do tablicy dla danego rzedu
					row[i] = war;
					i++;
				} else if (Value.getInstance(map.get(klucz).get(keykolumn).getTyp()) instanceof DateTimeValue) {
					row[i] = Value.getInstance(StringValue.class).create("		");
					i++;
				} else {
					row[i] = Value.getInstance(StringValue.class).create("		");
					i++;
				}
			}
			df.addrow(row);
			i = 0;
		}
		return df;
	}

    public DataFrame1 stdThread() {
        DataFrame1 df = new DataFrame1(colnames, colnamesTypes);
        Thread t = null;
        ArrayList<Thread> opList = new ArrayList<>();
        for (List<Value> key : map.keySet()) {
            t =new Thread( new OperationThread(df, new DataFrame1(map.get(key)), key.toString(), Operation.STD, groupByColnames));
            t.start();
            opList.add(t);
        }
        for(Thread tread : opList){
            try {
                tread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return df;
    }

	/**
	 * Analogicznie jak w mean() tylko bez dzielenia przez dlugosc i zwraca sume
	 * kolumny
	 */
	@Override
	public DataFrame1 sum() {
		int i = 0;
		DataFrame1 df = new DataFrame1(colnames, colnamesTypes);
		Value[] row = new Value[colnames.length];
		for (List<Value> klucz : map.keySet()) {
			Value suma = null;
			for (String keykolumn : map.get(klucz).map.keySet()) {
				if (Arrays.asList(groupByColnames).contains(keykolumn)) {
					row[i] = map.get(klucz).get(keykolumn).getArraylist().get(0);
					i++;
					continue;
				}
				if (!(Value.getInstance(map.get(klucz).get(keykolumn).getTyp()) instanceof DateTimeValue)
						&& !(Value.getInstance(map.get(klucz).get(keykolumn).getTyp()) instanceof StringValue)) {
					suma = Value.getInstance(map.get(klucz).get(keykolumn).getTyp()).create("0");
					for (Value elem : map.get(klucz).get(keykolumn).getArraylist()) {
						suma.add(elem);
					}
					row[i] = suma;
					i++;
				} else if (Value.getInstance(map.get(klucz).get(keykolumn).getTyp()) instanceof DateTimeValue) {
					row[i] = Value.getInstance(StringValue.class).create("		");
					i++;
				} else {
					row[i] = Value.getInstance(StringValue.class).create("		");
					i++;
				}
			}
			df.addrow(row);
			i = 0;
		}
		return df;
	}

    public DataFrame1 sumThread() {
        DataFrame1 df = new DataFrame1(colnames, colnamesTypes);
        Thread t = null;
        ArrayList<Thread> opList = new ArrayList<>();
        for (List<Value> key : map.keySet()) {
            t =new Thread( new OperationThread(df, new DataFrame1(map.get(key)), key.toString(), Operation.SUM, groupByColnames));
            t.start();
            opList.add(t);
        }
        for(Thread tread : opList){
            try {
                tread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return df;
    }

	/**
	 * Oblicza wariancje analogicznie jak w odchylenie
	 */
	@Override
	public DataFrame1 var() {

		int i = 0;
		DataFrame1 df = new DataFrame1(colnames, colnamesTypes);
		Value[] row = new Value[colnames.length];
		// petla ktora przechodzi po kazdej DataFrame1 w obiekcie klasy Divided
		// zapisanym w mapie
		for (List<Value> klucz : map.keySet()) {
			// suma przechowuje srednia danej kolumny
			Value suma = null;
			// war przechowuje wynik odchylenia
			Value war = null;
			// petla ktora przechodzi po kazdej Kolumnie w DataFrame1 a nastepnie oblicza
			// jej mean i na podstawie tego odchylenie
			for (String keykolumn : map.get(klucz).map.keySet()) {
				if (Arrays.asList(groupByColnames).contains(keykolumn)) {
					// System.out.println(Arrays.asList(colnames).contains(keykolumn) +" " +
					// colnames[0] + " "+ colnames[1]+ " "+keykolumn);
					row[i] = map.get(klucz).get(keykolumn).getArraylist().get(0);
					i++;
					continue;
				}
				// liczyby odchylenie tylko dla kolumn ktore nie sa typu String albo DataTime, w
				// przeciwnym razie przepisujemy tylko ich wartosc
				if (!(Value.getInstance(map.get(klucz).get(keykolumn).getTyp()) instanceof DateTimeValue)
						&& !(Value.getInstance(map.get(klucz).get(keykolumn).getTyp()) instanceof StringValue)) {
					// nadawanie wartosci 0 dla suma i war oraz tworzenie ich dla typu jakiego jest
					// kolumna
					suma = Value.getInstance(map.get(klucz).get(keykolumn).getTyp()).create("0");
					war = Value.getInstance(map.get(klucz).get(keykolumn).getTyp()).create("0");
					// liczenie sumy kolumny
					for (Value elem : map.get(klucz).get(keykolumn).getArraylist()) {
						suma.add(elem);
					}
					// liczenie sredniej kolumny
					suma.div(Value.getInstance(map.get(klucz).get(keykolumn).getTyp())
							.create(String.valueOf(map.get(klucz).size())));
					// liczenie wariancji kolumny
					for (Value elem : map.get(klucz).get(keykolumn).getArraylist()) {
                        try {
                            war.add((elem.clone().sub(suma)).mul(elem.clone().sub(suma)));
                        } catch (CloneNotSupportedException e) {
                            e.printStackTrace();
                        }
                    }
					war.div(Value.getInstance(map.get(klucz).get(keykolumn).getTyp())
							.create(String.valueOf(map.get(klucz).size())));
					// dodawania obliczonej wartosci do tablicy dla danego rzedu
					row[i] = war;
					i++;
				} else if (Value.getInstance(map.get(klucz).get(keykolumn).getTyp()) instanceof DateTimeValue) {
					row[i] = Value.getInstance(StringValue.class).create("		");
					i++;
				} else {
					row[i] = Value.getInstance(StringValue.class).create("		");
					i++;
				}
			}
			df.addrow(row);
			i = 0;
		}
		return df;
	}

    public DataFrame1 varThread() {
        DataFrame1 df = new DataFrame1(colnames, colnamesTypes);
        Thread t = null;
        ArrayList<Thread> opList = new ArrayList<>();
        for (List<Value> key : map.keySet()) {
            t =new Thread( new OperationThread(df, new DataFrame1(map.get(key)), key.toString(), Operation.VAR, groupByColnames));
            t.start();
            opList.add(t);
        }
        for(Thread tread : opList){
            try {
                tread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return df;
    }

	@Override
	public DataFrame1 apply(Applyable a) {
		DataFrame1 df = new DataFrame1(colnames, colnamesTypes);
		for (Map.Entry<List<Value>, DataFrame1> entry: map.entrySet()) {
			df.addrow(a.apply(entry.getValue()).getRow(0));
		}
		return df;
	}
	public enum Operation{
		MIN, MAX, SUM, MEAN, STD, VAR
	}
	public DataFrame1 operation(Operation op){
		switch(op){
			case MAX:
				return this.max();
			case MIN:
				return this.min();
			case SUM:
				return this.sum();
			case MEAN:
				return this.mean();
			case STD:
				return this.std();
			case VAR:
				return this.var();
			default:
				throw new IllegalArgumentException("No such operation");
		}
	}
}
