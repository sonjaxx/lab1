package Backend;

import Backend.Exceptions.DifferentColumnSizeException;
import Backend.ValueClasses.Value;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Klasa przechowywujaca arrayliste wartosci danego typu
 */
public class Kolumna implements Serializable {
	private String nazwa;
	private Class<? extends Value> typ;
	private ArrayList<Value> arraylist;
	transient private Logger logger = Logger.getLogger("MyLog");

	public Kolumna(String nazwa, Class<? extends Value> typy) {
		this.nazwa = nazwa;
		this.typ = typy;
		this.arraylist = new ArrayList<Value>();
	}

	public Class<? extends Value> getTyp() {
		return typ;
	}

	public void setTyp(Class<? extends Value> typ) {
		this.typ = typ;
	}

	/**
	 * getter
	 * 
	 * @return zwraca nazwe kolumny
	 */
	public String getKolumna() {
		return nazwa;
	}

	/**
	 * ustawia nazwe kolumny
	 * 
	 * @param nazwa
	 *            kolumny
	 */
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	/**
	 * 
	 * @return metoda zwraca rozmiar arraylisty
	 */
	public int arrayListSize() {
		return arraylist.size();
	}

	public ArrayList<Value> getArraylist() {
		return arraylist;
	}

	public void setArraylist(ArrayList<Value> arraylist) {
		this.arraylist = arraylist;
	}

	/**
	 * metoda dodajaca obiekt odpowiedniego typu(na podstawie typu kolumny) do
	 * arraylisty
	 * 
	 * @param str
	 *            - wartosc dodawanego obiektu
	 */
	public void AddValue(String str){
		Value v = Value.getInstance(typ).create(str);
		arraylist.add(v);
	}

	/**
	 * metoda dodajaca obiekt do arraylisty
	 * 
	 * @param v
	 *            = obiekt odpowiedniego typu
	 */
	public void AddValue(Value v) {
		arraylist.add(v);

	}

	/**
	 * 
	 * @param i
	 *            - index danego elementu arraylisty
	 * @return element znajdujacy sie na danej pozycji w arrayliscie
	 */
	public Value getValue(int i) {
		return arraylist.get(i);
	}

	public Kolumna addValueToCol(String s) {
		try {
		Value v = Value.getInstance(getTyp()).create(s);
		
		for(Value i : arraylist) {
			i.add(v);
		}}catch(NumberFormatException e) {
			System.out.println("Cannot add this value to column of type: " + typ);
		}
		return this;
	}
	public Kolumna subValueToCol(String s) {
		try {
		Value v = Value.getInstance(getTyp()).create(s);
		
		for(Value i : arraylist) {
			i.sub(v);
		}}catch(NumberFormatException e) {
			System.out.println("Cannot add this value to column of type: " + typ);
		}
		return this;
	}
	public Kolumna mulValueToCol(String s) {
		try {
		Value v = Value.getInstance(getTyp()).create(s);
		
		for(Value i : arraylist) {
			i.mul(v);
		}}catch(NumberFormatException e) {
			System.out.println("Cannot add this value to column of type: " + typ);
		}
		return this;
	}
	public Kolumna divValueToCol(String s) {
		try {
		Value v = Value.getInstance(getTyp()).create(s);
		
		for(Value i : arraylist) {
			i.div(v);
		}}catch(NumberFormatException e) {
			System.out.println("Cannot add this value to column of type: " + typ);
		}
		return this;
	}
	public Kolumna powValueToCol(String s) {
		try {
		Value v = Value.getInstance(getTyp()).create(s);
		
		for(Value i : arraylist) {
			i.pow(v);
		}}catch(NumberFormatException e) {
			System.out.println("Cannot add this value to column of type: " + typ);
		}
		return this;
	}

	public Kolumna addColToCol(Kolumna k) throws DifferentColumnSizeException{
		if(this.arrayListSize()!=k.arrayListSize()) {
			throw new DifferentColumnSizeException("Columns are different size:" + nazwa +"  " + arrayListSize() + " and "+ k.nazwa +"  " + k.arrayListSize());
		}
		try {
		for(int i = 0; i<this.arrayListSize(); i++) {
			this.getValue(i).add(k.getValue(i));
		}
	}
		catch(IllegalArgumentException e) {
		System.out.println(e.getMessage());
	}
		return this;
	}
	public Kolumna subColToCol(Kolumna k)throws DifferentColumnSizeException {
		if(this.arrayListSize()!=k.arrayListSize()) {
			throw new DifferentColumnSizeException("Columns are different size:" + nazwa +"  " + arrayListSize() + " and "+ k.nazwa +"  " + k.arrayListSize());
		}
		try {
		for(int i = 0; i<this.arrayListSize(); i++) {
			
			this.getValue(i).sub(k.getValue(i));
		}
		}
		catch(IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
		return this;
	}
	public Kolumna mulColToCol(Kolumna k) throws DifferentColumnSizeException{
		if(this.arrayListSize()!=k.arrayListSize()) {
			throw new DifferentColumnSizeException("Columns are different size:" + nazwa +"  " + arrayListSize() + " and "+ k.nazwa +"  " + k.arrayListSize());
		}
		try {
		for(int i = 0; i<this.arrayListSize(); i++) {
			this.getValue(i).mul(k.getValue(i));
		}
	}
		catch(IllegalArgumentException e) {
		System.out.println(e.getMessage());
	}
		return this;
	}
	public Kolumna divColToCol(Kolumna k)throws DifferentColumnSizeException {
		if(this.arrayListSize()!=k.arrayListSize()) {
			throw new DifferentColumnSizeException("Columns are different size:" + nazwa +"  " + arrayListSize() + " and "+ k.nazwa +"  " + k.arrayListSize());
		}
		try {
		for(int i = 0; i<this.arrayListSize(); i++) {
			this.getValue(i).div(k.getValue(i));
		}
	}
		catch(IllegalArgumentException e) {
		System.out.println(e.getMessage());
	}
		return this;
	}
	public Kolumna powColToCol(Kolumna k) throws DifferentColumnSizeException{
		if(this.arrayListSize()!=k.arrayListSize()) {
			throw new DifferentColumnSizeException("Columns are different size:" + nazwa +"  " + arrayListSize() + " and "+ k.nazwa +"  " + k.arrayListSize());
		}
		try {
		for(int i = 0; i<this.arrayListSize(); i++) {
			this.getValue(i).pow(k.getValue(i));
		}
	}
		catch(IllegalArgumentException e) {
		System.out.println(e.getMessage());
	}
		return this;
	}
	/**
	 * metoda zwracajaca reprezentacje arraylisty w postaci Stringa
	 */
	public String toString() {
		return arraylist.toString();
	}
}