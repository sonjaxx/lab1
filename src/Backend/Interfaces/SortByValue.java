package Backend.Interfaces;

import Backend.ValueClasses.Value;

import java.io.Serializable;
import java.util.Comparator;

public class SortByValue implements Comparator<Value>, Serializable {

	@Override
	public int compare(Value o1, Value o2) {
		// TODO Auto-generated method stub
		return (o1.lte(o2))? 1:-1;
	}

}
