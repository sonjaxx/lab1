package Backend.ValueClasses;

import java.io.Serializable;

public abstract class Value implements Cloneable, Serializable {
	 public int index;
	 public Value wartosc;

	public Value clone() throws CloneNotSupportedException {
		return (Value) super.clone();
	}

	public abstract String toString();

	public abstract Value add(Value v);

	public abstract Value sub(Value v);

	public abstract Value mul(Value v);

	public abstract Value div(Value v);

	public abstract Value pow(Value v);

	public abstract boolean eq(Value v);

	public abstract boolean lte(Value v);

	public abstract boolean gte(Value v);

	public abstract boolean neq(Value v);

	public abstract boolean equals(Object other);

	public abstract int hashCode();

	public abstract Value create(String s);

	public static Value getInstance(Class typ) {
		if (typ.getName().equals("Backend.ValueClasses.IntegerValue")) {
			return new IntegerValue();
		} else if (typ.getName().equals("Backend.ValueClasses.FloatValue")) {
			return new FloatValue();
		} else if (typ.getName().equals("Backend.ValueClasses.DoubleValue")) {
			return new DoubleValue();
		} else if (typ.getName().equals("Backend.ValueClasses.StringValue")) {
			return new StringValue();
		} else if (typ.getName().equals("Backend.ValueClasses.DateTimeValue")) {
			return new DateTimeValue();
		} else {
			return null;
		}
	};

}
