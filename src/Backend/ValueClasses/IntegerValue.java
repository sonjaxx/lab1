package Backend.ValueClasses;

public class IntegerValue extends Value {
	private Integer v;
	//private static IntegerValue instance = null;
	public IntegerValue(int v) {
		this.v = v;
	}
	public IntegerValue() {};

	public String toString() {
		return Integer.toString(v);
	}
	@Override
	public Value add(Value a) {
		if(a instanceof IntegerValue) {
			IntegerValue b = (IntegerValue)a;
			v += b.v;
		}
		return this;
	}
	public Value sub(Value a) {
		if(a instanceof IntegerValue) {
			IntegerValue b = (IntegerValue)a;
			v -= b.v;
		}
		return this;
	}
	public Value mul(Value a) {
		if(a instanceof IntegerValue) {
			IntegerValue b = (IntegerValue)a;
			v *= b.v;
		}
		return this;
	}
	public Value div(Value a) {
		if(a instanceof IntegerValue) {
			IntegerValue b = (IntegerValue)a;
			v /= b.v;
		}
		return this;
	}
	public Value pow(Value a) {
		if(a instanceof IntegerValue) {
			IntegerValue b = (IntegerValue)a;
			v = v^b.v;
		}
		return this;
	}
	public boolean eq(Value a) {
		if(a instanceof IntegerValue) {
			IntegerValue b = (IntegerValue)a;
			if(v==b.v) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	public boolean lte(Value a) {
		if(a instanceof IntegerValue) {
			IntegerValue b = (IntegerValue)a;
			if(b.v<v) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	public boolean gte(Value a) {
		if(a instanceof IntegerValue) {
			IntegerValue b = (IntegerValue)a;
			if(b.v>v) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	public boolean neq(Value a) {
		if(a instanceof IntegerValue) {
			IntegerValue b = (IntegerValue)a;
			if(v!=b.v) {
				return true;
			}
			else {
				return false;
			}
			//return (v!=b.v) ? true : false;
		}
		else {
			return false;
		}
	}
	public boolean equals(Object other) {
		return this==other;
	}
	public int hashCode() {
		return v.hashCode();
	}
	public Value create(String s) {
		return new IntegerValue((int)Double.parseDouble(s));
	}
	
}
