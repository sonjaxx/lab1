package Backend;

import Backend.ValueClasses.*;
import Backend.ValueClasses.Value;
import com.mysql.cj.xdevapi.Collection;

import java.io.*;
import java.sql.*;
import java.util.Arrays;

public class DataFrameDB extends DataFrame1{
    private Connection conn = null;
    private Statement stmt = null;
    private ResultSet rs = null;
    private String tableName;
    private String nameFile;
    public DataFrameDB(String[] kolumny, Class<? extends Value>[] typy, String nameFile){
        super(kolumny, typy);
        this.nameFile=nameFile;
        this.tableName = nameFile.split("\\.")[0];
        connect();

    }
    public void connect(){
        try {
            conn = DriverManager.getConnection("jdbc:mysql://mysql.agh.edu.pl:3306/sradon", "sradon","xwgFc1bj20Y7w0jE");

        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }catch(Exception e){e.printStackTrace();}
    }
    public void dropTable(){
        try {
            stmt = conn.createStatement();
            String strLine = "Drop table if exists " + tableName + ";";
            stmt.execute(strLine);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void readIn(String[] ColTypes){
        FileInputStream fstream = null;
        String strLine;
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder.append("CREATE TABLE IF NOT EXISTS ");
        stringbuilder.append(tableName);
        stringbuilder.append(" (");
        for(int i = 0; i<ColTypes.length; i++){
            stringbuilder.append(kolumny[i] + " " + ColTypes[i] + " NOT NULL, ");
        }
        stringbuilder.deleteCharAt(stringbuilder.length()-2);
        stringbuilder.append(" );");
        try {
            stmt = conn.createStatement();
            int res = stmt.executeUpdate(stringbuilder.toString());
            String loadQuery = "LOAD DATA LOCAL INFILE '" + nameFile +"' INTO TABLE " + tableName + " FIELDS TERMINATED BY ',' LINES TERMINATED BY '\\n' ignore 1 lines (";
            for(String name : kolumny){
                loadQuery += name + ",";
            }
            loadQuery=loadQuery.substring(0,loadQuery.length()-1);
            loadQuery+=" );";
            System.out.println(loadQuery);
            stmt.execute(loadQuery);


            System.out.println(res);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public DataFrame1 getDf(){
        String query = "SELECT * from " + tableName + ";";
        DataFrame1 df = new DataFrame1(this.kolumny, this.typy);
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            while(rs.next()){
                String s = "";
                for(int i = 1; i<=kolumny.length; i++){
                    s+=rs.getString(i)+",";
                }
                df.addrow(s);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    return df;
    }
    private static Class<? extends Value> convert(Integer idType){
        switch (idType){
            case 1:
                return StringValue.class;
            case 91:
                return DateTimeValue.class;
            case 8:
                return DoubleValue.class;
            case 6:
                return FloatValue.class;
            case 4:
                return IntegerValue.class;
                default:
                    return StringValue.class;
        }
    }
    public static DataFrame1 select(String sel){
        Connection conn = null;
        DataFrame1 df = null;
        try {
            conn = DriverManager.getConnection("jdbc:mysql://mysql.agh.edu.pl:3306/sradon", "sradon","xwgFc1bj20Y7w0jE");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sel);

            ResultSetMetaData rsmd = rs.getMetaData();
            int NumberOfCols = rsmd.getColumnCount();
            String colNames[] = new String [NumberOfCols];
            Class[] typy = new Class[NumberOfCols];
            for(int i = 1; i<=NumberOfCols; i++){
                colNames[i-1]=rsmd.getColumnName(i);
                typy[i-1] = convert(rsmd.getColumnType(i));
            }
            df  = new DataFrame1(colNames, typy);
            while(rs.next()){
                String s = "";
                for(int i = 1; i<=NumberOfCols; i++){
                    s+=rs.getString(i)+",";
                }
                df.addrow(s);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return df;
    }
    public DataFrame1 min(String[] groupByColnames){
        Connection conn = null;
        DataFrame1 df = null;

        StringBuilder sb = new StringBuilder();
        sb.append("select ");
        for(String s : kolumny){
            if (!Arrays.asList(groupByColnames).contains(s)){
                sb.append("min(" + s+"), ");
            }
        }
        sb.deleteCharAt(sb.length()-2);
        sb.append("from " + tableName + " group by ");
        for (String s : groupByColnames){
            sb.append(s + ", ");
        }
        sb.deleteCharAt(sb.length()-2);
        sb.append(";");
        System.out.println(sb.toString());


        try {
            conn = DriverManager.getConnection("jdbc:mysql://mysql.agh.edu.pl:3306/sradon", "sradon","xwgFc1bj20Y7w0jE");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sb.toString());

            ResultSetMetaData rsmd = rs.getMetaData();
            int NumberOfCols = rsmd.getColumnCount();
            String colNames[] = new String [NumberOfCols];
            Class[] typy = new Class[NumberOfCols];
            for(int i = 1; i<=NumberOfCols; i++){
                colNames[i-1]=rsmd.getColumnName(i);
                typy[i-1] = convert(rsmd.getColumnType(i));
            }
            df  = new DataFrame1(colNames, typy);
            while(rs.next()){
                String s = "";
                for(int i = 1; i<=NumberOfCols; i++){
                    s+=rs.getString(i)+",";
                }
                df.addrow(s);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return df;
    }
    public DataFrame1 max(String[] groupByColnames){
        Connection conn = null;
        DataFrame1 df = null;

        StringBuilder sb = new StringBuilder();
        sb.append("select ");
        for(String s : kolumny){
            if (!Arrays.asList(groupByColnames).contains(s)){
                sb.append("max(" + s+"), ");
            }
        }
        sb.deleteCharAt(sb.length()-2);
        sb.append("from " + tableName + " group by ");
        for (String s : groupByColnames){
            sb.append(s + ", ");
        }
        sb.deleteCharAt(sb.length()-2);
        sb.append(";");
        System.out.println(sb.toString());


        try {
            conn = DriverManager.getConnection("jdbc:mysql://mysql.agh.edu.pl:3306/sradon", "sradon","xwgFc1bj20Y7w0jE");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sb.toString());

            ResultSetMetaData rsmd = rs.getMetaData();
            int NumberOfCols = rsmd.getColumnCount();
            String colNames[] = new String [NumberOfCols];
            Class[] typy = new Class[NumberOfCols];
            for(int i = 1; i<=NumberOfCols; i++){
                colNames[i-1]=rsmd.getColumnName(i);
                typy[i-1] = convert(rsmd.getColumnType(i));
            }
            df  = new DataFrame1(colNames, typy);
            while(rs.next()){
                String s = "";
                for(int i = 1; i<=NumberOfCols; i++){
                    s+=rs.getString(i)+",";
                }
                df.addrow(s);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return df;
    }

}
