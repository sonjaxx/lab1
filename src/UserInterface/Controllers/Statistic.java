package UserInterface.Controllers;

import Backend.DataFrame1;
import Backend.ValueClasses.DateTimeValue;
import Backend.Divided;
import Backend.ValueClasses.StringValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

public class Statistic {
    private DataFrame1 df;
    @FXML
    private TabPane stat;
    private ArrayList<String> cols;
    private Divided.Operation operation;
    private ArrayList<String> names;
    public Statistic(DataFrame1 df, ArrayList<String> cols, Divided.Operation operation){
        this.df = df;
        this.cols = cols;
        this.operation = operation;
    }
    @FXML
    private void plot() throws IOException {
        String plotKolumn = stat.getSelectionModel().getSelectedItem().getText();
        PlotViewController plotViewController = new PlotViewController(df, plotKolumn, names);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../layouts/plotView.fxml"));
        loader.setController(plotViewController);
        AnchorPane root = loader.load();
        Scene sc = new Scene(root, 1000, 600);
        Stage st = new Stage();
        st.setTitle("Plot  " + plotKolumn);
        st.setScene(sc);            st.show();
    }
    @FXML
    void initialize(){
        Tab t ;
        ListView view;
        String string = "";
        names =new ArrayList<String>();
        for(int i=0;i< df.size();i++){
        for(String col : df.getKolumny()){
            if(cols.contains(col)) {
                string+=df.get(col).getValue(i).toString() + "  ";
            }
        }
        names.add(string);
        string = "";

            }
        ArrayList<String> values;
        for(String col : df.getKolumny()){
            if(!cols.contains(col)){
                if (!(df.get(col).getTyp() == DateTimeValue.class && df.get(col).getValue(0) instanceof StringValue) &&
                        (!df.get(col).getValue(0).toString().equals("		"))){
                values =new ArrayList<String>(names);
                for(int i=0;i< df.size();i++){
                    values.set(i, values.get(i) + df.get(col).getValue(i).toString());
                }
                t = new Tab(col );
                view = new ListView(FXCollections.observableArrayList(values));
//                hBox.getChildren().addAll(view,v);
                t.setContent(view);
                stat.getTabs().add(t);

            }}
        }

    }
}
