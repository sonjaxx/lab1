package Backend.ValueClasses;

/**
 * 
 * @author Sonia
 * Klasa dziedziczaca po Value przechwoujaca index i odpowiadajaca mu wartosc
 */
public final class COOValue extends Value {
	//public int index;
	//public Value wartosc;
	public COOValue(int index, Value wartosc) {
		this.index = index;
		this.wartosc = wartosc;
	}
	public COOValue() {};
/**
 * metoda zwraca wartosc obiektu jako String
 */
	public String toString() {
	    return wartosc.toString();
	}
	/**
	 * metoda dodajaca do siebie wartosci obiektow klasy COOValue
	 * @return obiekt COOValue
	 */
	public Value add(Value a) {
		if(a instanceof COOValue) {
			COOValue b = (COOValue)a;
			wartosc = wartosc.add(b.wartosc);
		}
		return this;
	}
	/**
	 * metoda odejmujaca od siebie wartosci obiektow klasy COOValue
	 */
	public Value sub(Value a) {
		if(a instanceof COOValue) {
			COOValue b = (COOValue)a;
			wartosc = wartosc.sub(b.wartosc);
		}
		return this;
	}
	/**
	 * metoda ktora mnozy przez siebie wartosci obiektow COOVAlue
	 */
	public Value mul(Value a) {
		if(a instanceof COOValue) {
			COOValue b = (COOValue)a;
			wartosc = wartosc.mul(b.wartosc);
		}
		return this;
	}
	/**
	 * metoda ktora dzieli wartosci obiektow klasy COOValue
	 */
	public Value div(Value a) {
		if(a instanceof COOValue) {
			COOValue b = (COOValue)a;
			wartosc = wartosc.div(b.wartosc);
		}
		return this;
	}
	/**
	 * metoda ktora podnosi wartosc obiektu COOValue do potegi wartosci przekazanego jako argument obiektu
	 */
	public Value pow(Value a) {
		if(a instanceof COOValue) {
			COOValue b = (COOValue)a;
			wartosc = wartosc.pow(b.wartosc);
		}
		return this;
	}
	/**
	 * metoda ktora sprawdza czy wartosci 2 obiektow COOValue sa sobie rowne
	 */
	public boolean eq(Value a) {
		if(a instanceof COOValue) {
			COOValue b = (COOValue)a;
			if(wartosc.eq(b.wartosc)) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	/**
	 * metoda sprawdzajaca czy wartosc obiektu przekazanego jako argument jest mniejsza niz wartosc obiektu COOValue
	 */
	public boolean lte(Value a) {
		if(a instanceof COOValue) {
			COOValue b = (COOValue)a;
			if(wartosc.lte(b.wartosc)) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	/**
	 * metoda sprawdzajaca czy wartosc obiektu przekazanego jako argument jest wieksza niz wartosc obiektu COOValue
	 */
	public boolean gte(Value a) {
		if(a instanceof COOValue) {
			COOValue b = (COOValue)a;
			if(wartosc.gte(b.wartosc)) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	/**
	 * metoda sprawdzajaca, czy wartosci 2 obiektow COOValue sa rozne
	 */
	public boolean neq(Value a) {
		if(a instanceof COOValue) {
			COOValue b = (COOValue)a;
			if(wartosc.neq(b.wartosc)) {
				return true;
			}
			else {
				return false;
			}
			//return (v!=b.v) ? true : false;
		}
		else {
			return false;
		}
	}
	/**
	 * metoda sprawdzajaca czy 2 obiekty sa takie same
	 */
	public boolean equals(Object other) {
		return this.equals(other);
	}
	/**
	 * 
	 */
	public int hashCode() {
		return this.wartosc.hashCode();
	}
	/**
	 * metoda tworzaca nowy obiekt danej klasy dla przekazanego argumentu
	 */
	public Value create(String s) {
		return this.wartosc.create(s);
	}

	
}
