package UserInterface.Controllers;

import Backend.DataFrame1;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.chart.*;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class PlotViewControllerDF {
    private DataFrame1 df;
    private String y;
    private String x;
    private Chart lineChart;
    private int ile;
    private String typechar;
    private double position;
    private Spinner<Integer> spinner;
    private SpinnerValueFactory.IntegerSpinnerValueFactory intFactory;
    private StringProperty displayPointProperty;
    @FXML
    private AnchorPane pane;
    public PlotViewControllerDF(DataFrame1 df, String x, String y, int ile, String typechar){
        this.df = df;
        this.y = y;
        this.x = x;
        this.ile = ile;
        this.typechar = typechar;
    }
    @FXML
    void initialize() throws NumberFormatException{
        System.out.println("Wykres");
        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel("Row Names");

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel(y);
        switch (typechar){
            case "LineChart" :
                lineChart = new LineChart(xAxis,yAxis);
                break;
            case "ScatterChart" :
                lineChart = new ScatterChart(xAxis, yAxis);
                break;
            case "BarChart" :
                lineChart = new BarChart(xAxis, yAxis);
                break;
            default :
                lineChart = new LineChart(xAxis,yAxis);
                break;
        }


        //odpowiada z ilosc danych na wykresie
        spinner = new Spinner<>();
        intFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(50, 10000, ile, 100);
        spinner.setValueFactory(intFactory);
        spinner.editableProperty().setValue(true);

        //odpowiada za poczatek wysietlanych danych
        ScrollBar scrollBar = new ScrollBar();
        Button button = new Button();
        button.setMinWidth(100);
        button.setText("Scroll");
        scrollBar.setMin(0);
        scrollBar.setMax(df.size()-ile);
        scrollBar.setUnitIncrement(1);


        VBox vBox = new VBox();
        HBox hBox = new HBox();
        //wysietla pozycje scrollbara
        Label displayPoint = new Label();
        displayPoint.prefWidth(250.0);
        displayPointProperty = new SimpleStringProperty();
        displayPoint.textProperty().bind(displayPointProperty);
        //listener dla buttona, pobiera wybrana pozycje scrollbara i wybrana ilosc danych i na tej podstawie generuje nowy wykres
        button.setOnAction(event -> {
            ile = intFactory.getValue();
            position = scrollBar.getValue();
            displayPointProperty.setValue("Position:      " + String.valueOf((int)position) + " - " + String.valueOf((int)(position + ile)));

            updateLineChart(position);
        });




        XYChart.Series dataSeries1 = new XYChart.Series();
        dataSeries1.setName(y);
        try {
            for (int i = 0; i < ile; i++) {
                dataSeries1.getData().add(new XYChart.Data(df.get(x).getValue(i).toString(), Double.valueOf(df.get(y).getValue(i).toString())));
            }
        }catch (NumberFormatException e){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Incorect plot");
            alert.setHeaderText(null);
            alert.setContentText("Sorry, we can't display plot for" + df.get(y).getTyp().toString() +  " objects on yaxis");
            alert.showAndWait();
        }

        ((XYChart)lineChart).getData().add(dataSeries1);



//        lineChart.setCreateSymbols(false);
        lineChart.prefWidthProperty().bind(pane.widthProperty());


        vBox.getChildren().add(lineChart);
        vBox.getChildren().add(scrollBar);


        hBox.getChildren().add(spinner);
        hBox.getChildren().add(button);
        hBox.getChildren().add(displayPoint);
        hBox.setSpacing(30);


        vBox.getChildren().add(hBox);

        pane.getChildren().add(vBox);


    }

    /**
     * metoda aktualizuje wyswietlane dane na wykresie
     * @param start
     */
    private void updateLineChart(double start){
        XYChart.Series dataSeries1 = new XYChart.Series();
        dataSeries1.setName(y);
        try {
            for (int i = (int)start ; i < (int)start + ile; i++) {
                dataSeries1.getData().add(new XYChart.Data(df.get(x).getValue(i).toString(), Double.valueOf(df.get(y).getValue(i).toString())));
            }
        }catch (NumberFormatException e){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Incorect plot");
            alert.setHeaderText(null);
            alert.setContentText("Sorry, we can't display plot for" + df.get(y).getTyp().toString() +  " objects on yaxis");
            alert.showAndWait();
        }
        ((XYChart)lineChart).getData().remove(0);
        ((XYChart)lineChart).getData().add(dataSeries1);
    }
}
