package Backend;

import Backend.Interfaces.SortByValue;
import Backend.ValueClasses.DateTimeValue;
import Backend.ValueClasses.StringValue;
import Backend.ValueClasses.Value;

import java.io.*;
import java.util.*;
import java.util.logging.Logger;

/**
 * 
 * @author Sonia Klasa przechowywujaca mape, ktora przyporzadkowuje danej nazwie
 *         kolumny jej zawartosc
 */
public class DataFrame1 implements Serializable {
	protected String[] kolumny;
	protected Class<? extends Value>[] typy;
	protected String str;
	protected boolean head = true;
	protected Map<String, Kolumna> map;


	private String[] groupByColnames = null;


	private Divided.Operation op = null;
	transient private Logger logger = Logger.getLogger("MyLog");


	/**
	 * Konstruktor tworzacy puste kolumny danego typu dla kazdej z podaych nazw
	 * 
	 * @param kolumny
	 *            - nazwy kolumn
	 * @param typy
	 *            - typy kolumn
	 */
	public DataFrame1(String[] kolumny, Class<? extends Value>[] typy) {
		if (kolumny.length != typy.length)
			throw new IllegalArgumentException("Number of column names is not the same sa number of types of columns");
		this.kolumny = kolumny;
		this.typy = typy;

		map = new LinkedHashMap<String, Kolumna>();
		for (int i = 0; i < kolumny.length; i++) {
			map.put(kolumny[i], new Kolumna(kolumny[i], typy[i]));
		}
	}

	/**
	 * Konstruktor wczytujacy dane z pliku do kolumn
	 * 
	 * @param str
	 *            - nazwa pliku
	 * @param typy
	 *            - typy kolumn
	 * @param head
	 *            - true jesli plik zawiera naglowki, false jesli nie
	 */
	public DataFrame1(String str, Class<? extends Value>[] typy, boolean head, int ile) {
		this.str = str;
		this.typy = typy;
		this.head = head;
		int a = 0;
		map = new LinkedHashMap<String, Kolumna>();
		try {

			FileInputStream fstream = new FileInputStream(str);
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String strLine;
			if (head == true) {
				kolumny = br.readLine().split(",");
				if (kolumny.length != typy.length) {
					br.close();
					throw new IllegalArgumentException(
							"Number of column names is not the same sa number of types of columns");
				}
				for (int i = 0; i < kolumny.length; i++) {
					map.put(kolumny[i], new Kolumna(kolumny[i], typy[i]));
				}
				while ((strLine = br.readLine()) != null && (a < ile || ile == 0)) {
					if (a % 100000 == 0) {
						System.out.println("Wczytano: " + a + " wierszy");
					}
					a++;
					try {
						addrow(strLine);
					} catch (ArrayIndexOutOfBoundsException e) {
						logger.info("Invalid number of columns in row:  " + a + "  the row was not added");
						System.out.println("Invalid number of columns in row:  " + a + "  the row was not added");
					}
				}
			} else {
				System.out.println("Podaj nazwy kolumn");
				Scanner scanner = new Scanner(System.in);
				for (int i = 0; i < typy.length; i++) {
					kolumny[i] = scanner.nextLine();
					map.put(kolumny[i], new Kolumna(kolumny[i], typy[i]));
				}
				scanner.close();
				if (kolumny.length != typy.length) {
					br.close();
					throw new IllegalArgumentException(
							"Number of column names is not the same sa number of types of columns");
				}

				while ((strLine = br.readLine()) != null && (a < ile || ile == 0)) {
					if (a % 500 == 0) {
						System.out.println("Wczytano: " + a + " wierszy");
					}
					a++;
					try {
						addrow(strLine);
					} catch (ArrayIndexOutOfBoundsException e) {
						logger.info("Invalid number of columns in row:  " + a);
						System.out.println("Invalid number of columns in row:  " + a);
					}

				}

			}
			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("Podales zla nazwe pliku, podaj go jeszcze raz");
			logger.info("Podales zla nazwe pliku, podaj go jeszcze raz");
			// e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.getMessage();
		}

		// finally {
		// br.close();
		// }

	}

	/**
	 * Konstruktor kopiujacy
	 * 
	 * @param df
	 *            - obiekt klasy DataFrame1
	 */
	public DataFrame1(DataFrame1 df) {
		kolumny = df.kolumny;
		typy = df.typy;
		map = new LinkedHashMap<String, Kolumna>(df.map);

	}
	public void setOp(Divided.Operation op) {
		this.op = op;
	}

	public Divided.Operation getOp() {
		return op;
	}
	public void setGroupByColnames(String[] groupByColnames) {
		this.groupByColnames = groupByColnames;
	}
	public String[] getKolumny() {
		return kolumny;
	}

	public void setKolumny(String[] kolumny) {
		this.kolumny = kolumny;
	}

	public Class<? extends Value>[] getTypy() {
		return typy;
	}

	public void setTypy(Class<? extends Value>[] typy) {
		this.typy = typy;
	}

	public Map<String, Kolumna> getMap() {
		return map;
	}

	public void setMap(Map<String, Kolumna> map) {
		this.map = map;
	}

	public void putColumn(Kolumna kolumna) {
		if (map.containsKey(kolumna.getKolumna())) {
			map.replace(kolumna.getKolumna(), kolumna);
		} else {
			map.put(kolumna.getKolumna(), kolumna);
		}
	}

	/**
	 * Metoda dodajaca wiersz do kolumny
	 * 
	 * @param row
	 *            - caly wiersz rozdzielony przecinkami
	 */
	public void addrow(String row) {
		String[] tab;
		int i = 0;
		tab = row.split(",");
		if (tab.length != map.keySet().size()) {
			throw new ArrayIndexOutOfBoundsException();
		}
		
			for (String key : map.keySet()) {
				try {
				map.get(key).AddValue(tab[i]);
				i++;}
				catch (NumberFormatException e) {
					map.get(key).AddValue("0");
					i++;
					//throw new NumberFormatException("Value: " + tab[i-1] +  "   could not be added to column of type:  " + map.get(key).getTyp() + "insted we added value 0.");
//					System.out.println("Value: " + tab[i-1] +  "   could not be added to column of type:  " + map.get(key).getTyp() + "insted we added value 0.");

					logger.info("Value: " + tab[i-1] +  "   could not be added to column of type:  " + map.get(key).getTyp() + "insted we added value 0.");
				}
			}
		}

	

	/**
	 * metoda dodajaca wiersz do kolumny
	 * 
	 * @param wartosci
	 *            - tablica obiektow dziedziczacych po Value
	 */
	public void addrow(Value[] wartosci) {
		int i = 0;
		for (String key : map.keySet()) {
			map.get(key).AddValue((wartosci[i]));
			i++;
		}
	}

	/**
	 * 
	 * @return metoda zwraca rozmiar kolumny
	 */
	public int size() throws NullPointerException {
		return map.get(kolumny[0]).arrayListSize();
	}

	// public void checkSize() {
	// int size = -1;
	// for (String key : map.keySet()) {
	// if (size == -1)
	// size = map.get(key).arrayListSize();
	// if (size != map.get(key).arrayListSize()) {
	// throw new IllegalArgumentException("Kolumny sa roznej dlugosci");
	// }
	// }
	// }

	/**
	 * 
	 * @param colname
	 *            - nazwa kolumny
	 * @return metoda zwraca obiekt klasy Kolumna
	 */
	public Kolumna get(String colname) {
		return map.get(colname);
	}

	/**
	 * metoda tworzaca kopie DataFrame dla wybranych kolumn
	 * 
	 * @param cols
	 *            - nazwy kolumn
	 * @param copy
	 *            - true jesli shallowcopy, false jesli deepcopy
	 * @return obiekt DataFrame1 z wybranymi kolumnami
	 */
	public DataFrame1 get(String[] cols, boolean copy) {
		DataFrame1 obiekt;
		Map<String, Kolumna> mapCopy = new HashMap<String, Kolumna>(map);
		if (copy) {
			obiekt = this;

		} else {
			obiekt = new DataFrame1(this);
		}
		boolean contain = false;

		for (String key : mapCopy.keySet()) {
			contain = false;
			for (String col : cols) {
				if (col.equals(key)) {
					contain = true;
				}

			}
			if (contain == false) {

				obiekt.map.remove(key);
			}
		}
		return obiekt;
	}

	/**
	 * 
	 * @param i
	 *            - numer wiersza
	 * @return metoda zwraca wiersz o zadanym indeksie
	 */
	public DataFrame1 iloc(int i) {
		DataFrame1 df3 = new DataFrame1(kolumny, typy);
		Value[] wartosci = new Value[kolumny.length];
		int j = 0;
		for (String key : map.keySet()) {
			wartosci[j] = map.get(key).getValue(i);
			j++;
		}
		df3.addrow(wartosci);
		return df3;
	}

	public Value[] getRow(int i) {
		Value[] row = new Value[kolumny.length];
		int j = 0;
		for (String key : map.keySet()) {
			row[j] = map.get(key).getValue(i);
			j++;
		}
		return row;
	}

	/**
	 * 
	 * @param from
	 *            - indeks wiersza poczatkowego
	 * @param to
	 *            - indeks wiersza koncowego
	 * @return metoda zwraca wiersze w podanym zakresie
	 */
	public DataFrame1 iloc(int from, int to) {
		DataFrame1 df3 = new DataFrame1(kolumny, typy);
		Value[] wartosci = new Value[kolumny.length];
		int j = 0;
		for (int i = from; i <= to; i++) {
			j = 0;
			for (String key : map.keySet()) {
				wartosci[j] = map.get(key).getValue(i);
				j++;
			}
			df3.addrow(wartosci);
		}

		return df3;
	}

	/**
	 * metoda zwracajaca reprezentacje obiektu DataFrame1 jako String( wszystkie
	 * kolumny)
	 */
	public String toString() {

		String wyswietl = "";
		for (String key : map.keySet()) {
			wyswietl += key + "		";
		}
		wyswietl += "\n";
		for (int i = 0; i < size(); i++) {
			for (String key : map.keySet()) {
				wyswietl += map.get(key).getArraylist().get(i) + "	";
			}
			wyswietl += "\n";
		}
		return wyswietl;
	}

	public Divided groupby(String[] colnames) {
		return new Divided(colnames, this);
	}

	public DataFrame1 operations(Divided.Operation op){
		Value suma = null;
		Value war = null;
		Value[] row = new Value[this.getKolumny().length];
		DataFrame1 df = new DataFrame1(kolumny, typy);
		int i =0;
		switch (op) {
			case MIN:
				for (String keyKolumn : this.getMap().keySet()) {
					Collections.sort(this.get(keyKolumn).getArraylist(), new SortByValue());
				}
				return this.iloc(0);
			case MAX:
				for (String keyKolumn : this.getMap().keySet()) {
					Collections.sort(this.get(keyKolumn).getArraylist(), new SortByValue());
				}
				return this.iloc(this.size() - 1);
			case MEAN:
				for (String keykolumn : this.getMap().keySet()) {
					if (Arrays.asList(groupByColnames).contains(keykolumn)) {
						row[i] = this.getMap().get(keykolumn).getArraylist().get(0);
						i++;
						continue;
					}
					if (!(Value.getInstance(this.get(keykolumn).getTyp()) instanceof DateTimeValue)
							&& !(Value.getInstance(this.get(keykolumn).getTyp()) instanceof StringValue)) {
						suma = Value.getInstance(this.get(keykolumn).getTyp()).create("0");
						for (Value elem : this.get(keykolumn).getArraylist()) {
							suma.add(elem);
						}
						suma.div(Value.getInstance(this.get(keykolumn).getTyp())
								.create(String.valueOf(this.size())));
						row[i] = suma;
						i++;
					} else if (Value.getInstance(this.get(keykolumn).getTyp()) instanceof DateTimeValue) {
						row[i] = Value.getInstance(StringValue.class).create("		");
						i++;
					} else {
						row[i] = Value.getInstance(StringValue.class).create("		");
						i++;
					}
				}
				df.addrow(row);
				return df;
			case STD:

				// std odpowiada za pierwiastkowanie wariancji, bedzie mialo wartosc 0.5
				Value std = null;
				// petla ktora przechodzi po kazdej Kolumnie w DataFrame1 a nastepnie oblicza
				// jej mean i na podstawie tego odchylenie
				for (String keykolumn : this.getMap().keySet()) {
					if (Arrays.asList(groupByColnames).contains(keykolumn)) {
						// System.out.println(Arrays.asList(colnames).contains(keykolumn) +" " +
						// colnames[0] + " "+ colnames[1]+ " "+keykolumn);
						row[i] = this.get(keykolumn).getArraylist().get(0);
						i++;
						continue;
					}
					// liczyby odchylenie tylko dla kolumn ktore nie sa typu String albo DataTime, w
					// przeciwnym razie przepisujemy tylko ich wartosc
					if (!(Value.getInstance(this.get(keykolumn).getTyp()) instanceof DateTimeValue)
							&& !(Value.getInstance(this.get(keykolumn).getTyp()) instanceof StringValue)) {
						// nadawanie wartosci 0 dla suma i war oraz tworzenie ich dla typu jakiego jest
						// kolumna
						suma = Value.getInstance(this.get(keykolumn).getTyp()).create("0");
						war = Value.getInstance(this.get(keykolumn).getTyp()).create("0");
						// std uzywana do podnoszenia wariancji do 0.5 i jest tego typu co kolumna
						std = Value.getInstance(this.get(keykolumn).getTyp()).create("0.5");
						// liczenie sumy kolumny
						for (Value elem : this.get(keykolumn).getArraylist()) {
							suma.add(elem);
						}
						// liczenie sredniej kolumny
						suma.div(Value.getInstance(this.get(keykolumn).getTyp())
								.create(String.valueOf(this.size())));
						// liczenie wariancji kolumny
						for (Value elem : this.get(keykolumn).getArraylist()) {
							try {
								war.add((elem.clone().sub(suma)).mul(elem.clone().sub(suma)));
							} catch (CloneNotSupportedException e) {
								e.printStackTrace();
							}
						}
						war.div(Value.getInstance(this.get(keykolumn).getTyp())
								.create(String.valueOf(this.size())));
						war.pow(std);
						// dodawania obliczonej wartosci do tablicy dla danego rzedu
						row[i] = war;
						i++;
					} else if (Value.getInstance(this.get(keykolumn).getTyp()) instanceof DateTimeValue) {
						row[i] = Value.getInstance(StringValue.class).create("		");
						i++;
					} else {
						row[i] = Value.getInstance(StringValue.class).create("		");
						i++;
					}
				}
				df.addrow(row);
				return df;
			case SUM:
				for (String keykolumn : this.getMap().keySet()) {
					if (Arrays.asList(groupByColnames).contains(keykolumn)) {
						row[i] = this.get(keykolumn).getArraylist().get(0);
						i++;
						continue;
					}
					if (!(Value.getInstance(this.get(keykolumn).getTyp()) instanceof DateTimeValue)
							&& !(Value.getInstance(this.get(keykolumn).getTyp()) instanceof StringValue)) {
						suma = Value.getInstance(this.get(keykolumn).getTyp()).create("0");
						for (Value elem : this.get(keykolumn).getArraylist()) {
							suma.add(elem);
						}
						row[i] = suma;
						i++;
					} else if (Value.getInstance(this.get(keykolumn).getTyp()) instanceof DateTimeValue) {
						row[i] = Value.getInstance(StringValue.class).create("		");
						i++;
					} else {
						row[i] = Value.getInstance(StringValue.class).create("		");
						i++;
					}
				}
				df.addrow(row);
				return df;
			case VAR:
				for (String keykolumn : this.getMap().keySet()) {
					if (Arrays.asList(groupByColnames).contains(keykolumn)) {
						// System.out.println(Arrays.asList(colnames).contains(keykolumn) +" " +
						// colnames[0] + " "+ colnames[1]+ " "+keykolumn);
						row[i] = this.get(keykolumn).getArraylist().get(0);
						i++;
						continue;
					}
					// liczyby odchylenie tylko dla kolumn ktore nie sa typu String albo DataTime, w
					// przeciwnym razie przepisujemy tylko ich wartosc
					if (!(Value.getInstance(this.get(keykolumn).getTyp()) instanceof DateTimeValue)
							&& !(Value.getInstance(this.get(keykolumn).getTyp()) instanceof StringValue)) {
						// nadawanie wartosci 0 dla suma i war oraz tworzenie ich dla typu jakiego jest
						// kolumna
						suma = Value.getInstance(this.get(keykolumn).getTyp()).create("0");
						war = Value.getInstance(this.get(keykolumn).getTyp()).create("0");
						// liczenie sumy kolumny
						for (Value elem : this.get(keykolumn).getArraylist()) {
							suma.add(elem);
						}
						// liczenie sredniej kolumny
						suma.div(Value.getInstance(this.get(keykolumn).getTyp())
								.create(String.valueOf(this.size())));
						// liczenie wariancji kolumny
						for (Value elem : this.get(keykolumn).getArraylist()) {
							try {
								war.add((elem.clone().sub(suma)).mul(elem.clone().sub(suma)));
							} catch (CloneNotSupportedException e) {
								e.printStackTrace();
							}
						}
						war.div(Value.getInstance(this.get(keykolumn).getTyp())
								.create(String.valueOf(this.size())));
						// dodawania obliczonej wartosci do tablicy dla danego rzedu
						row[i] = war;
						i++;
					} else if (Value.getInstance(this.get(keykolumn).getTyp()) instanceof DateTimeValue) {
						row[i] = Value.getInstance(StringValue.class).create("		");
						i++;
					} else {
						row[i] = Value.getInstance(StringValue.class).create("		");
						i++;
					}
				}
				df.addrow(row);
				return df;
			default:
				System.out.println("No such operation");
				return null;
		}
	}

}