package UserInterface.Controllers;

import Backend.DataFrame1;
import Backend.ValueClasses.DateTimeValue;
import Backend.ValueClasses.DoubleValue;
import Backend.ValueClasses.StringValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class ChooseFileController {
    @FXML private Button read;
    private List<String> list;
    private DataFrame1 df;
    @FXML private ComboBox combo;

    /**
     * metoda wywolana po nacisnieciu Button read, pobiera wybrana nazwe pliku i tworzy z niego DataFrame a nastepnie przenosi do sceny MainMenu
     * @param event
     * @throws IOException
     */
    @FXML
    protected void wczytaj(ActionEvent event) throws IOException{
        try {

            String fileName = combo.getSelectionModel().getSelectedItem().toString();
            Class [] c = new Class [4];
            c[0] = StringValue.class;
            c[1] = DoubleValue.class;
            c[2] = DoubleValue.class;
            c[3] = DoubleValue.class;
            df = new DataFrame1(fileName,c,true,0 );
            //creating new scene
            newStage("../layouts/mainMenu.fxml");
        }catch (NullPointerException e){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Item not selected");
            alert.setHeaderText(null);
            alert.setContentText("Please select the file name");
            alert.showAndWait();
        }

    }

    /**
     * tworzy nowa scene
     * @param FXMLscene nazwa pliku fxml dla nowej sceny
     * @throws IOException kiedy nie uda sie zaladowac pliku fxml
     */
    private void newStage(String FXMLscene) throws IOException {
        Stage stage = (Stage)read.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource(FXMLscene));
        Parent root =loader.load();
        MainMenu mm = loader.<MainMenu>getController();
        mm.setDataFrame(df);

        Scene scene = new Scene(root, 600, 275);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * metoda sprawdzajaca jakie pliki csv znajduja sie w katalogu projektu
     */
    private void getFileList(){
        File directory = new File(System.getProperty("user.dir"));
        list = new ArrayList<String>();
        for(String a : directory.list()){
            if(a.toLowerCase().endsWith(".csv")){
                list.add(a);
            }
        }
    }

    /**
     * kontruktor wywoluje tlyko metode getFileList
     */
    public ChooseFileController(){
        getFileList();
    }

    /**
     * przypisabnie wartosci poczatkowych dla sceny
     */
    @FXML
    void initialize() {
        combo.setItems(FXCollections.observableList(list));

//        read.setOnAction(event -> {
//            System.out.println("onAction");
//        });
//        EventHandler<MouseEvent> eventEventHandler = event -> {
//            System.out.println("Handler");
//        };
//
//        read.addEventHandler(MouseEvent.MOUSE_CLICKED, eventEventHandler);


    }

}
