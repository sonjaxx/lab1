package Backend.Interfaces;

import Backend.DataFrame1;

public interface Applyable {
	DataFrame1 apply(DataFrame1 a);
}
