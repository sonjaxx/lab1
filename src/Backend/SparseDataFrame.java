package Backend;

import Backend.ValueClasses.COOValue;
import Backend.ValueClasses.Value;

/**
 * 
 * @author Sonia
 *Klasa dziedziczaca po DataFrame1, ukrywa niepotrzebne pola
 */
public class SparseDataFrame extends DataFrame1 {
	private String hide;
	/**
	 * Konstruktor tworzy obiekt z pustymi kolumnami dla danych nazw i typow
	 * @param kolumny - nazwy kolumn
	 * @param typy - typy kolumn
	 * @param hide - wartosc ktora ma zostac ukryta
	 */
	public SparseDataFrame(String [] kolumny, Class<? extends Value> [] typy, String hide) {
		super(kolumny,typy);
		this.hide = hide;
	}
	/**
	 * konstruktor zmienia DataFrame1 na SparseDataFrame usuwajac wybrane pola
	 * @param data - obiekt DataFrame1
	 * @param hide - pole ktore ma zostac ukryte
	 */
	public SparseDataFrame(DataFrame1 data, String hide) {
		super(data.kolumny, data.typy);
		this.hide = hide;
		
		for(String key : data.map.keySet()) {
			this.putcolumn(data.map.get(key));
		}
		
	}
	/**
	 * metoda konwertuje kolumne na nowa kolumne pozbawiona wartosci rownych parametrowi hide. 
	 * Jesli nie bylo wczesniej takiej kolumny, zostanie ona dodana
	 * @param kolumna - kolumna ktora ma zostac zmieniona na Sparse
	 */
	public void putcolumn( Kolumna kolumna) {
		Kolumna kolumna2 = new Kolumna(kolumna.getKolumna(), COOValue.class);
		for(int i=0; i<kolumna.arrayListSize(); i++) {
			if(!kolumna.getValue(i).toString().equals(hide)) {
				kolumna2.AddValue(new COOValue(i, kolumna.getValue(i)));
			}
		}
		if (map.containsKey(kolumna.getKolumna())) {
			map.replace(kolumna.getKolumna(), kolumna2);
		} else {
			map.put(kolumna.getKolumna(), kolumna2);
		}
	}
	public DataFrame1 toDense() {
		DataFrame1 df = new DataFrame1(kolumny, typy);
		Kolumna kolumna;
		int index= 0;
		int indexStart = 0;
		for(String key: map.keySet())
		{
			kolumna = new Kolumna(key, typy[index]);
			for(int i = 0; i<map.get(key).arrayListSize(); i++) {
				while(indexStart< map.get(key).getValue(i).index)
				{
					indexStart ++;
					kolumna.AddValue(hide);
				}
				kolumna.AddValue(map.get(key).getValue(i).wartosc);
				indexStart  = map.get(key).getValue(i).index +1;
			}
			index++;
			df.putColumn(kolumna);
		}
		return df;
	}
	//public String toString() {
		//return "Jestem SparseDataFrame";
	//}


	
	
}
