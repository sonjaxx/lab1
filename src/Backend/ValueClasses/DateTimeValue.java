package Backend.ValueClasses;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DateTimeValue extends Value {
	private Date v;
	public DateTimeValue(Date v) {
		this.v = v;
	}
	public DateTimeValue() {};
	public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(v);
	}
	public Value add(Value a) {
		throw new UnsupportedOperationException();
	}
	public Value sub(Value a) {
		if(a instanceof DateTimeValue) {
			DateTimeValue b = (DateTimeValue)a;
			if(b.v.before(v)) {
				long mili = v.getTime() - b.v.getTime();
				IntegerValue i = new IntegerValue((int)(mili/(1000*3600*24)));
				return i;
						}
		return this;
		}else {
			throw new IllegalArgumentException();}
	}
	public Value mul(Value a) {
		throw new UnsupportedOperationException();
	}
	public Value div(Value a) {
		throw new UnsupportedOperationException();
	}
	public Value pow(Value a) {
		throw new UnsupportedOperationException();
	}
	public boolean eq(Value a) {
		if(a instanceof DateTimeValue) {
			DateTimeValue b = (DateTimeValue)a;
			if(v.compareTo(b.v)==0) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			throw new IllegalArgumentException();
		}
	}
	public boolean gte(Value a) {
		if(a instanceof DateTimeValue) {
			DateTimeValue b = (DateTimeValue)a;
			if(v.before(b.v)) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			throw new IllegalArgumentException();
		}
	}
	public boolean lte(Value a) {
		if(a instanceof DateTimeValue) {
			DateTimeValue b = (DateTimeValue)a;
			if(b.v.before(v)) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			throw new IllegalArgumentException();
		}
	}
	public boolean neq(Value a) {
		if(a instanceof DateTimeValue) {
			DateTimeValue b = (DateTimeValue)a;
			if(v.compareTo(b.v)!=0) {
				return true;
			}
			else {
				return false;
			}
			//return (v!=b.v) ? true : false;
		}
		else {
			throw new IllegalArgumentException();
		}
	}
	public boolean equals(Object other) {
		if(other instanceof DateTimeValue) {
			DateTimeValue b = (DateTimeValue)other;
			//System.out.println(b + "    " + this + "   " +((v.compareTo(b.v)==0)?true:false));
		return ((v.compareTo(b.v))==0?true:false);
		}else {
			return false;
//			throw new IllegalArgumentException();
		}
		
	}
	public int hashCode() {
		return v.hashCode();
	}
	public Value create(String s) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return new DateTimeValue(sdf.parse(s));
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
}
