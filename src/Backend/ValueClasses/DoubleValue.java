package Backend.ValueClasses;

public class DoubleValue extends Value {
	private Double v;
	public DoubleValue(Double v) {
		this.v = v;
	}
	public DoubleValue() {}

	public String toString() {
		return Double.toString(v);
	}
	public Value add(Value a) {
		if(a instanceof DoubleValue) {
			DoubleValue b = (DoubleValue)a;
			v += b.v;
		return this;
		}else {
			throw new IllegalArgumentException("U can't add this Values: " + this.getClass() + "   and     " + a.getClass());}
	}
	public Value sub(Value a) {
		if(a instanceof DoubleValue) {
			DoubleValue b = (DoubleValue)a;
			v -= b.v;
			return this;
		}else {
			throw new IllegalArgumentException("U can't sub this Values: " + this.getClass() + "	and		" + a.getClass() );}
	}
	public Value mul(Value a){
		if(a instanceof DoubleValue) {
			DoubleValue b = (DoubleValue)a;
			v *= b.v;
			return this;
		}else {
			throw new IllegalArgumentException();}
	}
	public Value div(Value a){
		if(a instanceof DoubleValue) {
			DoubleValue b = (DoubleValue)a;
			v /= b.v;
			return this;
		}else {
			throw new IllegalArgumentException();}
	}
	public Value pow(Value a){
		if(a instanceof DoubleValue) {
			DoubleValue b = (DoubleValue)a;
			v = Math.pow(v, b.v);
			return this;
		}else {
			throw new IllegalArgumentException();}
	}
	public boolean eq(Value a) {
		if(a instanceof DoubleValue) {
			DoubleValue b = (DoubleValue)a;
			if(v==b.v) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			throw new IllegalArgumentException();
		}
	}
	public boolean lte(Value a) {
		if(a instanceof DoubleValue) {
			DoubleValue b = (DoubleValue)a;
			if(b.v<v) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			throw new IllegalArgumentException();
		}
	}
	public boolean gte(Value a){
		if(a instanceof DoubleValue) {
			DoubleValue b = (DoubleValue)a;
			if(b.v>v) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			throw new IllegalArgumentException();
		}
	}
	public boolean neq(Value a){
		if(a instanceof DoubleValue) {
			DoubleValue b = (DoubleValue)a;
			if(v!=b.v) {
				return true;
			}
			else {
				return false;
			}
			//return (v!=b.v) ? true : false;
		}
		else {
			throw new IllegalArgumentException();
		}
	}
	public boolean equals(Object other) {
		if(other instanceof DoubleValue) {
			DoubleValue b = (DoubleValue)other;
			return v.equals(b.v);
		}
			else {
				return false;
			}
	}
	public int hashCode() {
		return v.hashCode();
	}
	public Value create(String s) {
		return new DoubleValue(Double.parseDouble(s));
	}
}
