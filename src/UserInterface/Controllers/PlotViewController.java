package UserInterface.Controllers;

import Backend.DataFrame1;
import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;

import java.util.ArrayList;

public class PlotViewController {
    private DataFrame1 groupedDF;
    private String col;
    private ArrayList<String> a;
    @FXML
    private AnchorPane pane;
    public PlotViewController(DataFrame1 groupedDF, String col, ArrayList<String> a){
        this.groupedDF = groupedDF;
        this.col = col;
        this.a = a;
    }

    /**
     * tworzy taby dla odpowiednich kolumn i wypenia je wartosciami kolumn
     * @throws NumberFormatException
     */
    @FXML
    void initialize() throws NumberFormatException{
        //okresla typ osi x
        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel("Row Names");
        //okresla typ osi y
        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel(col);
        LineChart lineChart = new LineChart(xAxis, yAxis);
        //nowa seria danych
        XYChart.Series dataSeries1 = new XYChart.Series();
        dataSeries1.setName(col);
        //wypelnianie seri danych
        try {
            for (int i = 0; i < groupedDF.size(); i++) {
                dataSeries1.getData().add(new XYChart.Data(a.get(i), Double.valueOf(groupedDF.get(col).getValue(i).toString())));
            }
        }catch (NumberFormatException e){

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Incorect plot");
            alert.setHeaderText(null);
            alert.setContentText("Sorry, we can't display plot for" + groupedDF.get(col).getTyp().toString() +  " objects on yaxis");
            alert.showAndWait();
        }
        lineChart.getData().add(dataSeries1);
        lineChart.prefWidthProperty().bind(pane.widthProperty());
       pane.getChildren().add(lineChart);





    }


}
