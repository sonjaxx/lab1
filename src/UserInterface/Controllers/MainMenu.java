package UserInterface.Controllers;

import Backend.DataFrame1;
import Backend.Divided;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;


public class MainMenu {
    private DataFrame1 df;

    /**
     * metoda ustawia obiekt DataFrame dla nowej sceny
     * @param df - obiekt DataFrame z poprzedniej sceny
     */
    public void setDataFrame(DataFrame1 df){
        this.df = df;
        ArrayList<CheckBox> arr = new ArrayList<CheckBox>();
        for(String colname : df.getKolumny()){
            arr.add(new CheckBox(colname));
        }
        listproperty.set(FXCollections.observableArrayList(arr));
        listPropertyX.set(FXCollections.observableArrayList(df.getKolumny()));
    }
    @FXML
    private Button create;
    @FXML
    private ComboBox<String> chart_type;

    @FXML
    private ComboBox<Divided.Operation> oper;

    @FXML
    private ListView<CheckBox> list;
    @FXML
    private ComboBox<String> x;
    @FXML
    private Spinner<Integer> spinner;
    private SpinnerValueFactory.IntegerSpinnerValueFactory intFactory;
    @FXML
    private ComboBox<String> y;
    @FXML
    private Button wykres;
    private ListProperty<CheckBox> listproperty = new SimpleListProperty<CheckBox>();
    private ListProperty<String> listPropertyX = new SimpleListProperty<String>();
    private ArrayList<String> colnames;
    private Divided.Operation o;
    private int ile;

    /**
     *metoda przpieta pod button create, pobiera wartosci z listview z kolumnami po ktorych mamy grupowac oraz operacje z comboboxa, nastepnie grupuje i wywoluje dana operacje i przenosi do nowej sceny statistic
     * @param event
     */
    @FXML
    protected void generate(ActionEvent event) {
        //lista na nazwy kolumn po ktorych grupujemy
        colnames = new ArrayList<String>();
        CheckBox checkbox ;
        //for ktory sprawdza ktore kolumny zostaly wybrane i dodaje je do colnames
        for(Object boxy : list.getItems()){
            checkbox = (CheckBox)boxy;
            if(checkbox.isSelected()==true){
                colnames.add(checkbox.getText());
            }
        }
        o = oper.getSelectionModel().getSelectedItem();
        //jak nie wybierzemy operacji to alert
        try {
        DataFrame1 d = df.groupby(colnames.toArray(new String[colnames.size()])).operation(o);
        System.out.println(d);

            newStage2("../layouts/statistic.fxml", d);
        } catch (IOException e) {
            e.printStackTrace();
        }catch (NullPointerException e){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Item not selected");
        alert.setHeaderText(null);
        alert.setContentText("Please select the operation");
        alert.showAndWait();
    }
    }

    /**
     * metoda pobiera wartosci z comboboxow dla jakich kolumn ma byc wykres a nastepnie tworzy nowego stage
     * @param event
     * @throws IOException-wyjatek jak nie uda sie zaladowac pliku fxml
     */
    @FXML
    void makePlot(ActionEvent event) throws IOException {
        String xColumn = x.getSelectionModel().getSelectedItem();
        String yColumn = y.getSelectionModel().getSelectedItem();
        String chartype = chart_type.getSelectionModel().getSelectedItem();
        try{
        if (xColumn == null || yColumn == null || chartype == null){
            throw new NullPointerException();
        }
        ile = intFactory.getValue();
        PlotViewControllerDF plotViewController = new PlotViewControllerDF(df, xColumn, yColumn, ile, chartype);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../layouts/plotView.fxml"));
        loader.setController(plotViewController);
        AnchorPane root = loader.load();
        Scene sc = new Scene(root, 1000, 600);
        Stage st = new Stage();
        st.setTitle("Plot  " + yColumn);
        st.setScene(sc);
        st.show();
        }catch (NullPointerException e){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Item not selected");
            alert.setHeaderText(null);
            alert.setContentText("Please select the column");
            alert.showAndWait();
        }

    }

    /**
     * metoda tworzy nowy stage
     * @param name-nazwa pluku fxml dla staga
     * @param d - pogrupowana dataframe
     * @throws IOException
     */
    private void newStage2(String name,DataFrame1 d) throws IOException {
        Statistic s = new Statistic(d,colnames, o);
        FXMLLoader loader = new FXMLLoader(getClass().getResource(name));
        loader.setController(s);
        AnchorPane root = loader.load();
        Scene sc = new Scene(root, 600,600);
        Stage st = new Stage();
        st.setTitle("Statystyki od :" + o.toString());
        st.setScene(sc);
        st.show();

    }
    @FXML
    private void lazyLoad() throws IOException {
        lazyLoadController llc = new lazyLoadController(df);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../layouts/lazyLoad.fxml"));
        loader.setController(llc);
        AnchorPane root = loader.load();
        Scene sc = new Scene(root, 1000,600);
        Stage st = new Stage();
        st.setTitle("DataFrame");
        st.setScene(sc);
        st.show();
        llc.bindscroll();
    }
    @FXML
    private void showLogs() throws IOException {
        ShowLogsController showLogsController = new ShowLogsController();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../layouts/showLogs.fxml"));
        loader.setController(showLogsController);
        AnchorPane root = loader.load();
        Scene sc = new Scene(root, 600,400);
        Stage st = new Stage();
        st.setTitle("Errors");
        st.setScene(sc);
        st.show();
    }
    /**
     * pzypisuje poczatkowe wartosci
     */
    @FXML
    void initialize(){
        oper.setItems(FXCollections.observableArrayList(Divided.Operation.values()));
        list.itemsProperty().bind(listproperty);
        x.itemsProperty().bind(listPropertyX);
        y.itemsProperty().bind(listPropertyX);


        intFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(6, 10000, 150, 100);
        spinner.setValueFactory(intFactory);
        spinner.editableProperty().setValue(true);
        ArrayList<String> a = new ArrayList<String>();
        a.add("LineChart");
        a.add("ScatterChart");
        a.add("BarChart");
        chart_type.setItems(FXCollections.observableList(a));

    }
}
