package Backend.Threads;

import Backend.DataFrame1;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class CalcSeverThread implements Runnable {
    private DataFrame1 dfToSend;
    private DataFrame1 dfRespoonse;
    private int port = 6670;
    private String host = "localhost";
    public CalcSeverThread(DataFrame1 dfToSend, DataFrame1 dfResponse){
        this.dfToSend = dfToSend;
        this.dfRespoonse = dfResponse;
    }
    @Override
    public void run() {
        try {
            Socket s = new Socket(host, port);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(s.getOutputStream());
            ObjectInputStream objectInputStream = new ObjectInputStream(s.getInputStream());
            objectOutputStream.writeObject(dfToSend);
            DataFrame1 df = (DataFrame1)objectInputStream.readObject();
            dfRespoonse.addrow(df.getRow(0));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
