package UserInterface.Controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;


import java.io.*;

public class ShowLogsController {
    @FXML
    AnchorPane anchorpane;
    private TextArea textArea;
    private VBox vBox;
    @FXML
    void initialize(){
        String strLine;
        textArea = new TextArea();
//        vBox = new VBox(textArea);
        try {
            StringBuilder stringBuilder = new StringBuilder();
            FileInputStream fstream = new FileInputStream("error.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            while ((strLine = br.readLine())!= null){
                stringBuilder.append(strLine);
                stringBuilder.append("\n");
            }
            textArea.setText(stringBuilder.toString());
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        textArea.setPrefWidth(anchorpane.getPrefWidth());
        textArea.setPrefHeight(anchorpane.getPrefHeight());
        anchorpane.getChildren().add(textArea);
    }


}
