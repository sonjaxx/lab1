package Backend;

import Backend.Sockets.Client;
import Backend.ValueClasses.DateTimeValue;
import Backend.ValueClasses.DoubleValue;
import Backend.ValueClasses.StringValue;

class DataFrameProgram{
public static void main(String[] args) {
	Class [] c = new Class [4];
	c[0] = StringValue.class;
	c[1] = DateTimeValue.class;
	c[2] = DoubleValue.class;
	c[3] = DoubleValue.class;
	//System.out.println(c[3]);
	DataFrame1 df = new DataFrame1("groupby.csv", c, true , 0);
	//System.out.println(df);
	String[] kols	= new String[1];
	kols[0] = "id";
	//kols[1] = "date";
	//kols[2] = "total";
	//kols[3] = "val";
	Divided d = df.groupby(kols);
	Client a = new Client(d, Divided.Operation.STD);
	Client.sendToServer(a);
//    String[] group	= new String[1];
//    group[0] = "id";
//	String[] colTypes = new String[4];
//	colTypes[0] = "char(1)";
//	colTypes[1] = "date";
//	colTypes[2] = "double";
//	colTypes[3] = "double";
//	long start = System.nanoTime();
//	System.out.println(df.groupby(kols).std());
//	long stop = System.nanoTime();
//	long start2 = System.nanoTime();
//	System.out.println(df.groupby(kols).stdThread());
//	long stop2 = System.nanoTime();
////	System.out.println(df3);
//	System.out.println(stop-start);
//	System.out.println(stop2-start2);
 //DataFrameDB db = new DataFrameDB(kols, c, "groupby.csv");

// for (Divided.Operation op : Divided.Operation.values()){
//     	long start = System.nanoTime();
//	    DataFrame1 df2 = df.groupby(group).operation(op);
//	    long stop = System.nanoTime();
//	    long czas = stop-start;
//	    System.out.println(op.toString() + czas);
// }
//    long start = System.nanoTime();
//    df.groupby(group).minThread();
//    long stop = System.nanoTime();
//    long czas = stop-start;
//    System.out.println("Min:" + czas);
//
//    start = System.nanoTime();
//    df.groupby(group).maxThread();
//    stop = System.nanoTime();
//    czas = stop-start;
//    System.out.println("Max:" + czas);
//
//    start = System.nanoTime();
//    df.groupby(group).stdThread();
//    stop = System.nanoTime();
//    czas = stop-start;
//    System.out.println("Std:" + czas);
//
//    start = System.nanoTime();
//    df.groupby(group).varThread();
//    stop = System.nanoTime();
//    czas = stop-start;
//    System.out.println("Var:" + czas);
//
//    start = System.nanoTime();
//    DataFrame1 df2 = df.groupby(group).meanThread();
//    stop = System.nanoTime();
//    czas = stop-start;
//    System.out.println("Mean:" + czas);
//
//    start = System.nanoTime();
//    df.groupby(group).sumThread();
//    stop = System.nanoTime();
//    czas = stop-start;
//    System.out.println("Sum:" + czas);
//
//
//
//    start = System.nanoTime();
//    db.min(group);
//    stop = System.nanoTime();
//    czas = stop-start;
//    System.out.println("Min baza:" + czas);
//
//    start = System.nanoTime();
//    db.max(group);
//    stop = System.nanoTime();
//    czas = stop-start;
//    System.out.println("Max baza:" + czas);
//
//
//    start = System.nanoTime();
//    DataFrameDB.select("select std(total), std(val) from groupby group by id;");
//    stop = System.nanoTime();
//    czas = stop-start;
//    System.out.println("Std baza:" + czas);
//
//    start = System.nanoTime();
//    DataFrameDB.select("select variance(total), variance(val) from groupby group by id;");
//    stop = System.nanoTime();
//    czas = stop-start;
//    System.out.println("Var baza:" + czas);
//
//    start = System.nanoTime();
//    DataFrameDB.select("select sum(total), sum(val) from groupby group by id;");
//    stop = System.nanoTime();
//    czas = stop-start;
//    System.out.println("Sum baza:" + czas);
//
//    start = System.nanoTime();
//    DataFrameDB.select("select avg(total), avg(val) from groupby group by id;");
//    stop = System.nanoTime();
//    czas = stop-start;
//    System.out.println("Mean baza:" + czas);


// db.dropTable();
  // db.readIn(colTypes);
//	DataFrame1 d = db.getDf();
//	System.out.println(d.getClass());
//	System.out.println(d.iloc(5));

//	String zapytanie = "Select  avg(total), avg(val),avg(id) from groupby group by id;";
//	long start = System.nanoTime();
//	DataFrame1 df2 = DataFrameDB.select(zapytanie);
//	long stop = System.nanoTime();
//	System.out.println(df2);
//	long start2 = System.nanoTime();
//	DataFrame1 df3 = df.groupby(kols).mean();
//	long stop2 = System.nanoTime();
//	System.out.println(df3);
//	System.out.println(stop-start);
//	System.out.println(stop2-start2);


 }
}