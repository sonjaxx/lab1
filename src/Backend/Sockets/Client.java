package Backend.Sockets;

import Backend.DataFrame1;
import Backend.Divided;
import javafx.scene.chart.XYChart;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client implements Serializable {
    public Divided d;
    private Divided.Operation op;
    public Client(Divided d, Divided.Operation op){
        this.d=d;
        this.op=op;
    }
    public static void sendToServer(Client c){
        Socket echoSocket = null;
        ObjectOutputStream ous = null;
        ObjectInputStream objectInputStream = null;

        try{
            echoSocket = new Socket("192.168.0.249", 6666);
            ous = new ObjectOutputStream(echoSocket.getOutputStream());
            ous.writeObject(c);
            objectInputStream = new ObjectInputStream(echoSocket.getInputStream());
            DataFrame1 df = (DataFrame1) objectInputStream.readObject();
                    System.out.println(df);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    public Divided.Operation getOp() {
        return op;
    }
}
