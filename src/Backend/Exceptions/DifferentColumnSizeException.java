package Backend.Exceptions;

public class DifferentColumnSizeException extends Exception{
	public DifferentColumnSizeException(String mes) {
		super(mes);
	}

}
