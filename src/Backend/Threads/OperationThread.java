package Backend.Threads;

import Backend.DataFrame1;
import Backend.Divided;
import Backend.Interfaces.SortByValue;
import Backend.ValueClasses.DateTimeValue;
import Backend.ValueClasses.StringValue;
import Backend.ValueClasses.Value;

import java.util.Arrays;
import java.util.Collections;

public class OperationThread implements Runnable{
    private DataFrame1 toSort;
    private DataFrame1 df;
    private String threadName;
    private Thread t;
    private Divided.Operation operation;
    private String[] groupByColnames;
//    Value suma = null;
//    Value war = null;
//    Value[] row;
//    int i =0;
    public OperationThread(DataFrame1 df, DataFrame1 toSort, String threadName, Divided.Operation operation){
        this.toSort=toSort;
        this.threadName=threadName;
        this.df=df;
        this.operation = operation;
//        row = new Value[this.df.getKolumny().length];
    }
    public OperationThread(DataFrame1 df, DataFrame1 toSort, String threadName, Divided.Operation operation, String[] groupByColnames){
        this.toSort=toSort;
        this.threadName=threadName;
        this.df=df;
        this.operation = operation;
        this.groupByColnames = groupByColnames;
//        row = new Value[this.df.getKolumny().length];
    }

    @Override
    public void run() {
    Value suma = null;
    Value war = null;
    Value[] row = new Value[this.df.getKolumny().length];
    int i =0;

        switch (operation) {
            case MIN:
                for (String keyKolumn : toSort.getMap().keySet()) {
                    Collections.sort(toSort.get(keyKolumn).getArraylist(), new SortByValue());
                }
                df.addrow(toSort.getRow(0));
                break;
            case MAX:
                for (String keyKolumn : toSort.getMap().keySet()) {
                    Collections.sort(toSort.get(keyKolumn).getArraylist(), new SortByValue());
                }
                df.addrow(toSort.getRow(toSort.size() - 1));
                break;
            case MEAN:
//                        Value[] row = new Value[df.getKolumny().length];
//                        int i =0;
//                        Value suma = null;
                for (String keykolumn : toSort.getMap().keySet()) {
                    if (Arrays.asList(groupByColnames).contains(keykolumn)) {
                        row[i] = toSort.getMap().get(keykolumn).getArraylist().get(0);
                        i++;
                        continue;
                    }
                    if (!(Value.getInstance(toSort.get(keykolumn).getTyp()) instanceof DateTimeValue)
                            && !(Value.getInstance(toSort.get(keykolumn).getTyp()) instanceof StringValue)) {
                        suma = Value.getInstance(toSort.get(keykolumn).getTyp()).create("0");
                        for (Value elem : toSort.get(keykolumn).getArraylist()) {
                            suma.add(elem);
                        }
                        suma.div(Value.getInstance(toSort.get(keykolumn).getTyp())
                                .create(String.valueOf(toSort.size())));
                        row[i] = suma;
                        i++;
                    } else if (Value.getInstance(toSort.get(keykolumn).getTyp()) instanceof DateTimeValue) {
                        row[i] = Value.getInstance(StringValue.class).create("		");
                        i++;
                    } else {
                        row[i] = Value.getInstance(StringValue.class).create("		");
                        i++;
                    }
                }
                df.addrow(row);
                i = 0;
                break;
            case STD:

                // std odpowiada za pierwiastkowanie wariancji, bedzie mialo wartosc 0.5
                Value std = null;
                // petla ktora przechodzi po kazdej Kolumnie w DataFrame1 a nastepnie oblicza
                // jej mean i na podstawie tego odchylenie
                for (String keykolumn : toSort.getMap().keySet()) {
                    if (Arrays.asList(groupByColnames).contains(keykolumn)) {
                        // System.out.println(Arrays.asList(colnames).contains(keykolumn) +" " +
                        // colnames[0] + " "+ colnames[1]+ " "+keykolumn);
                        row[i] = toSort.get(keykolumn).getArraylist().get(0);
                        i++;
                        continue;
                    }
                    // liczyby odchylenie tylko dla kolumn ktore nie sa typu String albo DataTime, w
                    // przeciwnym razie przepisujemy tylko ich wartosc
                    if (!(Value.getInstance(toSort.get(keykolumn).getTyp()) instanceof DateTimeValue)
                            && !(Value.getInstance(toSort.get(keykolumn).getTyp()) instanceof StringValue)) {
                        // nadawanie wartosci 0 dla suma i war oraz tworzenie ich dla typu jakiego jest
                        // kolumna
                        suma = Value.getInstance(toSort.get(keykolumn).getTyp()).create("0");
                        war = Value.getInstance(toSort.get(keykolumn).getTyp()).create("0");
                        // std uzywana do podnoszenia wariancji do 0.5 i jest tego typu co kolumna
                        std = Value.getInstance(toSort.get(keykolumn).getTyp()).create("0.5");
                        // liczenie sumy kolumny
                        for (Value elem : toSort.get(keykolumn).getArraylist()) {
                            suma.add(elem);
                        }
                        // liczenie sredniej kolumny
                        suma.div(Value.getInstance(toSort.get(keykolumn).getTyp())
                                .create(String.valueOf(toSort.size())));
                        // liczenie wariancji kolumny
                        for (Value elem : toSort.get(keykolumn).getArraylist()) {
                            try {
                                war.add((elem.clone().sub(suma)).mul(elem.clone().sub(suma)));
                            } catch (CloneNotSupportedException e) {
                                e.printStackTrace();
                            }
                        }
                        war.div(Value.getInstance(toSort.get(keykolumn).getTyp())
                                .create(String.valueOf(toSort.size())));
                        war.pow(std);
                        // dodawania obliczonej wartosci do tablicy dla danego rzedu
                        row[i] = war;
                        i++;
                    } else if (Value.getInstance(toSort.get(keykolumn).getTyp()) instanceof DateTimeValue) {
                        row[i] = Value.getInstance(StringValue.class).create("		");
                        i++;
                    } else {
                        row[i] = Value.getInstance(StringValue.class).create("		");
                        i++;
                    }
                }
                df.addrow(row);
                i = 0;
                break;
            case SUM:
                for (String keykolumn : toSort.getMap().keySet()) {
                    if (Arrays.asList(groupByColnames).contains(keykolumn)) {
                        row[i] = toSort.get(keykolumn).getArraylist().get(0);
                        i++;
                        continue;
                    }
                    if (!(Value.getInstance(toSort.get(keykolumn).getTyp()) instanceof DateTimeValue)
                            && !(Value.getInstance(toSort.get(keykolumn).getTyp()) instanceof StringValue)) {
                        suma = Value.getInstance(toSort.get(keykolumn).getTyp()).create("0");
                        for (Value elem : toSort.get(keykolumn).getArraylist()) {
                            suma.add(elem);
                        }
                        row[i] = suma;
                        i++;
                    } else if (Value.getInstance(toSort.get(keykolumn).getTyp()) instanceof DateTimeValue) {
                        row[i] = Value.getInstance(StringValue.class).create("		");
                        i++;
                    } else {
                        row[i] = Value.getInstance(StringValue.class).create("		");
                        i++;
                    }
                }
                df.addrow(row);
                i = 0;
            case VAR:
                for (String keykolumn : toSort.getMap().keySet()) {
                    if (Arrays.asList(groupByColnames).contains(keykolumn)) {
                        // System.out.println(Arrays.asList(colnames).contains(keykolumn) +" " +
                        // colnames[0] + " "+ colnames[1]+ " "+keykolumn);
                        row[i] = toSort.get(keykolumn).getArraylist().get(0);
                        i++;
                        continue;
                    }
                    // liczyby odchylenie tylko dla kolumn ktore nie sa typu String albo DataTime, w
                    // przeciwnym razie przepisujemy tylko ich wartosc
                    if (!(Value.getInstance(toSort.get(keykolumn).getTyp()) instanceof DateTimeValue)
                            && !(Value.getInstance(toSort.get(keykolumn).getTyp()) instanceof StringValue)) {
                        // nadawanie wartosci 0 dla suma i war oraz tworzenie ich dla typu jakiego jest
                        // kolumna
                        suma = Value.getInstance(toSort.get(keykolumn).getTyp()).create("0");
                        war = Value.getInstance(toSort.get(keykolumn).getTyp()).create("0");
                        // liczenie sumy kolumny
                        for (Value elem : toSort.get(keykolumn).getArraylist()) {
                            suma.add(elem);
                        }
                        // liczenie sredniej kolumny
                        suma.div(Value.getInstance(toSort.get(keykolumn).getTyp())
                                .create(String.valueOf(toSort.size())));
                        // liczenie wariancji kolumny
                        for (Value elem : toSort.get(keykolumn).getArraylist()) {
                            try {
                                war.add((elem.clone().sub(suma)).mul(elem.clone().sub(suma)));
                            } catch (CloneNotSupportedException e) {
                                e.printStackTrace();
                            }
                        }
                        war.div(Value.getInstance(toSort.get(keykolumn).getTyp())
                                .create(String.valueOf(toSort.size())));
                        // dodawania obliczonej wartosci do tablicy dla danego rzedu
                        row[i] = war;
                        i++;
                    } else if (Value.getInstance(toSort.get(keykolumn).getTyp()) instanceof DateTimeValue) {
                        row[i] = Value.getInstance(StringValue.class).create("		");
                        i++;
                    } else {
                        row[i] = Value.getInstance(StringValue.class).create("		");
                        i++;
                    }
                }
                df.addrow(row);
                i = 0;
                break;
            default:
                System.out.println("No such operation");
        }
    }
}
