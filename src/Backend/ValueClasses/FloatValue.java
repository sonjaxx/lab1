package Backend.ValueClasses;

public class FloatValue extends Value {
	private Float v;
	//private static FloatValue instance = null;
	public FloatValue(float v) {
		this.v = v;
	}
	public FloatValue() {};

	public String toString() {
		return Float.toString(v);
	}
	public Value add(Value a) {
		if(a instanceof FloatValue) {
			FloatValue b = (FloatValue)a;
			v += b.v;
			return this;
		}else {
		throw new IllegalArgumentException();}
	}
	public Value sub(Value a) {
		if(a instanceof FloatValue) {
			FloatValue b = (FloatValue)a;
			v -= b.v;
			return this;
		}
		else {
		throw new IllegalArgumentException();}
	}
	public Value mul(Value a) {
		if(a instanceof FloatValue) {
			FloatValue b = (FloatValue)a;
			v *= b.v;
			return this;
		}else {
		throw new IllegalArgumentException();}
	}
	public Value div(Value a) {
		if(a instanceof FloatValue) {
			FloatValue b = (FloatValue)a;
			if(b.v == 0.0) {
				throw new IllegalArgumentException();
			}
			v /= b.v;
			return this;
		}else {
		throw new IllegalArgumentException();}
	}
	public Value pow(Value a) {
		if(a instanceof FloatValue) {
			FloatValue b = (FloatValue)a;
			v = (float)Math.pow(v,b.v);
			return this;
		}else {
		throw new IllegalArgumentException();}
	}
	public boolean eq(Value a) {
		if(a instanceof FloatValue) {
			FloatValue b = (FloatValue)a;
			if(v==b.v) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			throw new IllegalArgumentException();
		}
	}
	public boolean lte(Value a) {
		if(a instanceof FloatValue) {
			FloatValue b = (FloatValue)a;
			if(b.v<v) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			throw new IllegalArgumentException();
		}
	}
	public boolean gte(Value a) {
		if(a instanceof FloatValue) {
			FloatValue b = (FloatValue)a;
			if(b.v>v) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			throw new IllegalArgumentException();
		}
	}
	public boolean neq(Value a) {
		if(a instanceof FloatValue) {
			FloatValue b = (FloatValue)a;
			if(v!=b.v) {
				return true;
			}
			else {
				return false;
			}
			//return (v!=b.v) ? true : false;
		}
		else {
			throw new IllegalArgumentException();
		}
	}
	public boolean equals(Object other) {
		if(other instanceof FloatValue) {
			FloatValue b = (FloatValue)other;
			return v.equals(b.v);
		}
			else {
				return false;
			}
	}
	public int hashCode() {
		return v.hashCode();
	}
	public Value create(String s) {
		return new FloatValue(Float.parseFloat(s));
	}
}
