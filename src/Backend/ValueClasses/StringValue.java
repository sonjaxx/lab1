package Backend.ValueClasses;

import java.io.Serializable;

public class StringValue extends Value  {
	private String v;
	public StringValue(String v) {
		this.v = v;
	}
	public StringValue() {};
	public String toString() {
		return v;
	}
	public Value add(Value a) {
		if(a instanceof StringValue) {
			StringValue b = (StringValue)a;
			v += b.v;
			return this;
		}else {
			throw new IllegalArgumentException();}
	}
	public Value sub(Value a) {
		if(a instanceof StringValue) {
			StringValue b = (StringValue)a;
			v = v.replace(b.v, "");
			return this;
		}else {
			throw new IllegalArgumentException();}
	}
	public Value mul(Value a) {
		throw new UnsupportedOperationException();
	}
	public Value div(Value a) {
		throw new UnsupportedOperationException();
	}
	public Value pow(Value a) {
		throw new UnsupportedOperationException();
	}
	@Override
	public boolean eq(Value a) {
		if(a instanceof StringValue) {
			StringValue b = (StringValue)a;
			if(v.equals(b.v)) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	public boolean lte(Value a) {
		if(a instanceof StringValue) {
			StringValue b = (StringValue)a;
			if(b.v.compareTo(this.v)<0) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			throw new IllegalArgumentException();
		}
	}
	public boolean gte(Value a) {
		if(a instanceof StringValue) {
			StringValue b = (StringValue)a;
			if(b.v.compareTo(this.v)>0) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			throw new IllegalArgumentException();
		}
	}
	public boolean neq(Value a) {
		if(a instanceof StringValue) {
			StringValue b = (StringValue)a;
			if(!v.equals(b.v)) {
				return true;
			}
			else {
				return false;
			}
			//return (v!=b.v) ? true : false;
		}
		else {
			throw new IllegalArgumentException();
		}
	}
	public boolean equals(Object other) {
		if(other instanceof StringValue) {
			StringValue b = (StringValue)other;
			//System.out.println(b + "    " + this + "   " +((v.compareTo(b.v)==0)?true:false));
			return v.equals(b.toString());
		}else {
			return false;
//			throw new IllegalArgumentException();
		}

	}
	public int hashCode() {
		return v.hashCode();
	}
	public Value create(String s) {
		return new StringValue(s);
	}
}
